<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>我的收藏</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
        Header start
    ***********************************-->
    <%@include file="head.jsp" %>
    <!--**********************************
        Header end ti-comment-alt
    ***********************************-->

    <!--**********************************
        Sidebar start
    ***********************************-->
    <%@include file="left_nav.jsp" %>

    <!--**********************************
        Sidebar end
    ***********************************-->

    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">我的收藏</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">账号管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="email-left-box" style="width: 500px"><a href="${bp}/employee/tocollect.action?bmid=-99&aid=${currentUser.aid}" class="btn btn-primary btn-block">收藏夹</a>
                                <form method="post" action="${bp}/employee/deletebookmarks.action" id="form1">
                                    <div class="mail-list mt-4">
                                        <a href="${bp}/employee/toaddbookmarks.action" class="list-group-item border-0 p-r-0"><i
                                                class="fa fa-paper-plane font-18 align-middle mr-2"></i>新建</a>
                                    </div>
                                    <div class="list-group mail-list">
                                        <c:forEach items="${listbook}" var="bookmarks">
                                            <div class="message" style="margin-top: 8px">
                                                <div class="col-mail col-mail-1">
                                                    <span style="width: 50px;float: left"><a href="${bp}/employee/tocollect.action?aid=${currentUser.aid}&bmid=${bookmarks.bmid}" type="text"  style="margin-top: 0">${bookmarks.bmid}</a></span>
                                                    <span style="width: 100px;float: left"  class="subject" style="margin-left: 35px;font-size: 20px" onclick="window.location.href='${bp}/employee/tocollect.action?aid=${currentUser.aid}&bmid=${bookmarks.bmid}'" ><a style="font-size: 15px">${bookmarks.bmname}</a></span>
                                                    <span style="width: 70px;float: right;margin-top: 12px"><a style="padding: 0;font-size: 15px;cursor: pointer" class="list-group-item border-0 p-r-0" onclick="mydeleteone(${bookmarks.bmid})"><i class="fa fa-trash font-18 align-middle mr-2"></i>删除</a></span>
                                                    <span style="width: 70px;float: right;margin-top: 12px "><a style="padding: 0;font-size: 15px" class="mdi mdi-file-document-box font-18 align-middle mr-2" style="margin-left: 10px;" href="${bp}/employee/toupdatebookmarks.action?bmid=${bookmarks.bmid}">修改</a></span>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </form>
                            </div>
                            <div class="container-fluid">
                                <div class="row" >
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <a class="list-group-item border-0 p-r-0" onclick="mydeletecol()"><i
                                                        class="fa fa-trash font-18 align-middle mr-2"></i></a>
                                                <div class="table-responsive">
                                                    <form id="form2" method="post" action="${bp}/employee/deletecollect.action">
                                                        <input name="aid" value="${currentUser.aid}" type="hidden">
                                                        <table class="table table-striped table-bordered zero-configuration" width="500px">
                                                            <thead>
                                                            <tr>
                                                                <th width="10%"><input type="checkbox"></th>
                                                                <th>内容标题</th>
                                                                <th width="20%">操作</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <%--          int eid, String ename, String esex, Date birthday, String tel, String address, Role role, Dept dept, Estate estate, Date entrydate--%>
                                                            <c:forEach items="${list}" var="listcollect">
                                                                <c:forEach var="collect" items="${listcollect}">
                                                                        <tr>
                                                                            <td><input type="checkbox" id="chk6" name="idss" value="${collect.cid}"></td>
                                                                            <td><a href="${collect.study.stuaddr}">${collect.study.stutitle}</a></td>
                                                                            <td>
                                                                                <a href="${bp}/employee/toupdatecollect.action?cid=${collect.cid}&aid=${currentUser.aid}">修改</a>
                                                                                <a  style="margin-left: 40px;cursor: pointer"
                                                                                   onclick="deleteone(${collect.cid})">删除</a>
                                                                            </td>
                                                                        </tr>
                                                                    </a>
                                                                </c:forEach>
                                                            </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>
<script>

    //收藏内容的批量删除功能
    function mydeletecol() {
        var checks = $('input[type="checkbox"]:checked');
        console.log(checks);
        var check_all = $("#check-all");
        var flag = check_all.is(":checked");
        console.log(checks.length);
        console.log(flag);
        if (checks.length > 1 || (checks.length == 1 && flag == false)) {
            var flag = confirm("确定要删除吗?")
            if (flag) {
                $("#form2").submit();
            }
        } else {
            alert("至少要选中一条数据!");
        }
    }

    //单条删除收藏内容
    function deleteone(idss) {
        if (confirm("确定删除该内容吗?")) {
            window.location.href = "${bp}/employee/deletecollect.action?aid=${currentUser.aid}&idss=" + idss;
        }
    }

    //删除收藏夹
    function mydeleteone(bmid){
        if (confirm("确定删除该内容吗?")) {
            window.location.href = "${bp}/employee/deletebookmarks.action?bmid=" + bmid;
        }
    }

</script>

</body>

</html>