<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>永远不要停下学习的脚步</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
        Nav header start
    ***********************************-->

    <!--**********************************
        Nav header end
    ***********************************-->

    <!--**********************************
        标题开始
    ***********************************-->
    <%@include file="head.jsp" %>
    <!--**********************************
        Header end ti-comment-alt
    ***********************************-->

    <!--**********************************
        导航栏开始
    ***********************************-->
    <%@include file="left_nav.jsp" %>
    <!--**********************************
        导航栏结束
    ***********************************-->

    <!--**********************************
        主要内容开始
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">学习内容</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">学习管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <!-- End Row -->
            <div class="row">

                <div class="col-12 m-b-30">
                    <div class="row">
                        <form id="form1" action="${bp}/personnel/deletestudy.action" method="post"></form>
                        <c:forEach var="study" items="${list}">
                            <div class="col-md-6 col-lg-3">
                                <div class="card" style="width: 378px;height: 330px">
                                    <a href="${study.stuaddr}" target="_blank"><img style="width: 378px;height: 237px"
                                                                                    class="img-fluid"
                                                                                    src="${study.imgaddr}"
                                                                                    alt="123"></a>
                                    <div class="card-body">
                                        <div style="float: left"><h5 class="card-title"><a
                                                href="${study.stuaddr}" target="_blank">${study.stutitle}</a></h5>
                                        </div>
                                        <c:if test="${currentUser.employeeVO.role.rid==3 || currentUser.employeeVO.dept.did==5}">
                                        <div style="float:right;width: 62px">
                                            <button type="button" class="btn btn-danger px-3 ml-4"
                                                    value="${study.stuid}" onclick="myfun(this.value)"
                                                    style="cursor: pointer;color: white">删除
                                            </button>
                                        </div>
                                        </c:if>
                                        <div style="float:right;width: 62px;margin-right: 10px">
                                            <button type="button" class="btn btn-primary px-3 ml-4"
                                                    value="${study.stuid}"
                                                    onclick="checkstuid(this.value)" style="cursor: pointer" title="收藏">
                                                收藏
                                            </button>
                                        </div>

                                    </div>

                                    <input type="hidden" name="stuid" value="${study.stuid}">
                                    <span class="mui-icon mui-icon-star mui-pull-right"></span>
                                </div>
                            </div>
                        </c:forEach>
                        <!-- End Col -->
                    </div>
                </div>
            </div>


        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        主要内容结束
    ***********************************-->


</div>
<!--**********************************
    主界面结束
***********************************-->

<!--**********************************
    脚本
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>
<script>
    function checkstuid(stuid) {
        var aid =${currentUser.aid};
        console.log(stuid);
        console.log(aid);
        $.post("${bp}/employee/checkstuid.action", {"stuid": stuid, "aid": aid}, function (data) {
            console.log(data);
            if (data == true) {
                alert("该内容已收藏");
                <%--window.location.href = "${bp}/toaddcollect.action?stuid=${study.stuid}&aid=${currentUser.aid}";--%>
            } else {
                window.location.href = "${bp}/employee/toaddcollect.action?aid=${currentUser.aid}&stuid=" + stuid;
            }
        });
    }

    function myfun(stuid) {
        var flag = confirm("确定要删除该内容吗？")
        if (flag == true) {
            window.location.href = "${bp}/personnel/deletestudy.action?stuid=" + stuid;
        }
    }
</script>
</body>
</html>