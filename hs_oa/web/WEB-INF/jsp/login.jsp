<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>

<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>欢迎体验</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"> -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        body{
            background-image:url(${bp}/statics/images/my/login4.png);
            background-repeat: no-repeat;background-attachment: fixed;
            background-size: cover;
        }
        input:-ms-input-placeholder{
            color: #fff;
        }
        input:-moz-placeholder{
            color: #fff;
        }
        input::-moz-placeholder{
            color: #fff;
        }
        input::placeholder{
            color: #fff;
        }
    </style>
</head>

<body class="h-100">


<div style="height: 500px;margin-top: 220px">
            <div style="border-radius: 5%; width:1000px;height: 400px;margin: 0 auto;background-color:rgba(0,0,0,0.5);" >
                <div style="margin-top: 50px;float: left" >
                        <div style="padding-left:50px;width:400px;color: #fff;">
                            <h4 style="color: #fff;"><b>选择角色:</b></h4>
                            <div style="width: 150px;float: left;font-size: 16px">
                                <ul class="nav flex-column nav-pills">
                                    <li ><a style="cursor: pointer;"  data-toggle="pill" class="nav-link "name="type" value="1" onclick="myfun(1)">董事长</a></li>
                                    <li><a style="cursor: pointer" data-toggle="pill" class="nav-link" name="type" value="2"onclick="myfun(2)">人事部</a></li>
                                    <li><a style="cursor: pointer" data-toggle="pill" class="nav-link"name="type" value="3"
                                           onclick="myfun(3)">研发部</a></li>
                                    <li> <a style="cursor: pointer" data-toggle="pill" class="nav-link"name="type" value="5"
                                            onclick="myfun(5)">部门经理</a></li>
                                    <li> <a style="cursor: pointer" data-toggle="pill" class="nav-link"name="type" value="4"
                                            onclick="myfun(4)">普通员工</a></li>
                                    <li> <a style="cursor: pointer" data-toggle="pill" class="nav-link"name="type" value="6"
                                            onclick="myfun(6)">已离职员工</a></li>
                                    <li> <a style="cursor: pointer" data-toggle="pill" class="nav-link active"onclick="window.location.href='${bp}/index.action'">回到主页</a></li>
                                </ul>
                            </div>

                            <div style="padding:0 20px;margin-top:50px;width: 200px;font-size: 16px;float: left">
                                <div  style="width: 150px">
                                    <div id="account" style="width: 150px;" class="tab-pane fade active show">
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div style="width:400px; margin-left:20px;float: right">
                    <div class="form-input-content">
                        <div class="card login-form mb-0" style="background-color:transparent;">
                            <div  class="card-body pt-5" >
                                <a class="text-center" href="${bp}/index.action"> <h3 style="color: #fff"><b>华晟科技</b></h3></a>
                                <form class="mt-5 mb-5 login-input" action="${bp}/login.action">
                                    <div class="form-group">
                                        <input  style="color: #fff;" type="text" name="name" class="form-control" required
                                               value="${accountVO.aid}"
                                               placeholder="请输入电话或者账号...">
                                    </div>
                                    <div class="form-group">
                                        <input style="color: #fff;" type="password" name="pass" class="form-control" required
                                               value="${accountVO.pass}"
                                               placeholder="请输入密码...">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn login-form__btn submit w-100" type="submit">登录</button>
                                        <p style="color: red">${mess}</p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>


    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <script>
        function myfun(type) {
            console.log(type)
            $.post("${bp}/getaccount.action", {"type": type}, function (data) {
                $("#account").html("<div>账号:"+data.aid+"</div><div>密码:"+data.pass+"</div>");
            });
        }
    </script>

</body>
</html>





