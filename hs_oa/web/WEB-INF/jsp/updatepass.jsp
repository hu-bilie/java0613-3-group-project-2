<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>

<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>修改密码</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">
    
</head>

<body class="h-100">


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">
    <%@include file="head.jsp"%>

    <%@include file="left_nav.jsp"%>
    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">修改密码</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">个人信息</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="login-form-bg h-100">
           <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
        
                                <form class="mt-5 mb-5 login-input" id="form1" action="${bp}/employee/updatepass.action" method="post">
                                    <div class="form-group">
                                        <input type="text" name="aid" class="form-control" value="${aid}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" id="pass" name="pass" class="form-control"  placeholder="请输入新密码..." required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" id="pass1" name="pass1" class="form-control" placeholder="请确认密码..." required>
                                    </div>
                                    <button class="btn login-form__btn submit w-100" type="button" onclick="myfun()">确认修改</button>
                                </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
    <script>
        function myfun(){
            var pass = $("#pass").val();
            var pass1 = $("#pass1").val();
            if (pass!=pass1){
                alert("两次密码输入不一致!");
                return false;
            }
            $("#form1").submit()
        }
    </script>

</body>
</html>





