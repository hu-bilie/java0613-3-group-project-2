<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<%--商标公共页面--%>
<div style="width: 1900px">
    <div style="width: 15%;float: left">
        <a href="${bp}/login.action?name=${currentUser.aid}&pass=${currentUser.pass}">
            <img src="images/logo.png" width="240px" height="80px">
        </a>
    </div>
    <div class="header">
        <div class="header-content clearfix">
            <div class="nav-control">
                <div class="hamburger">
                    <span class="toggle-icon"><i class="icon-menu"></i></span>
                </div>
            </div>
            <div class="header-right">

                <ul class="clearfix">
                    <c:if test="${currentUser.employeeVO.role.rid==3}">
                        <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                            <i class="mdi mdi-airplay"></i>
                            <c:if test="${messagenum>0}">
                                <span class="badge gradient-1 badge-pill badge-primary">${messagenum}</span>
                            </c:if>
                        </a>
                            <div class="drop-down animated fadeIn dropdown-menu">
                                <div class="dropdown-content-heading d-flex justify-content-between">
                                    <c:if test="${messagenum>0}">
                                        <span class="">合作通知:${messagenum}条新信息</span>
                                    </c:if>
                                    <c:if test="${messagenum==0}">
                                        <span class="">暂无新信息</span>
                                    </c:if>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <c:forEach items="${messagelist}" var="message">
                                            <li class="notification-unread">
                                                <a href="javascript:void()">
                                                    <img class="float-left mr-3 avatar-img" src="images/avatar/1.jpg"
                                                         alt="">
                                                    <div class="notification-content">
                                                        <div class="notification-heading"><a href="${bp}/leader/messageinfo.action?id=${message.id}">${message.name}</a></div>
                                                        <div class="notification-heading"><a href="${bp}/leader/messageinfo.action?id=${message.id}">${message.subject}</a></div>
                                                        <div class="notification-heading"><a href="${bp}/leader/messageinfo.action?id=${message.id}">${message.submittime}</a></div>
                                                    </div>
                                                </a>
                                            </li>
                                        </c:forEach>
                                    </ul>

                                </div>
                            </div>
                        </li>
                    <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                        <i class="mdi mdi-email-outline"></i>
                        <c:if test="${num>0}">
                            <span class="badge gradient-1 badge-pill badge-primary">${num}</span>
                        </c:if>
                    </a>
                        <div class="drop-down animated fadeIn dropdown-menu">
                            <div class="dropdown-content-heading d-flex justify-content-between">
                                <c:if test="${num>0}">
                                    <span class="">审批:${num}条新信息</span>
                                </c:if>
                                <c:if test="${num==0}">
                                    <span class="">暂无审批信息</span>
                                </c:if>
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <li class="notification-unread">
                                        <a href="javascript:void()">
                                            <img class="float-left mr-3 avatar-img" src="images/avatar/1.jpg"
                                                 alt="">
                                            <div class="notification-content">
                                                <c:if test="${leavenum>0}">
                                                    <div class="notification-heading"><a
                                                            href="${bp}/employee/toleavelist.action?type=3&eid=0">请假审批:${leavenum}条新信息</a>
                                                    </div>
                                                </c:if>
                                                <c:if test="${projectnum>0}">
                                                    <div class="notification-heading"><a
                                                            href="${bp}/rad/toprojectlist.action?type=3&eid=0">项目审批:${projectnum}条新信息</a>
                                                    </div>
                                                </c:if>
                                                <c:if test="${expensenum>0}">
                                                    <div class="notification-heading"><a
                                                            href="${bp}/employee/toexpenselist.action?type=3&eid=0">报销审批:${expensenum}条新信息</a>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </li>
                    </c:if>
                    <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                        <i class="mdi mdi-bell-outline"></i>
                        <c:if test="${meetingnum>0}">
                            <span class="badge badge-pill gradient-2 badge-primary">${meetingnum}</span>
                        </c:if>
                    </a>
                        <div class="drop-down animated fadeIn dropdown-menu dropdown-notfication">
                            <div class="dropdown-content-heading d-flex justify-content-between">
                                <c:if test="${meetingnum>0}">
                                    <span class="">${meetingnum}条会议通知</span>
                                </c:if>
                                <c:if test="${meetingnum==0}">
                                    <span class="">暂无通知</span>
                                </c:if>
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <c:forEach items="${meetings}" var="meetings">
                                        <li>
                                            <a href="${bp}/employee/tomeetinginfo.action?mid=${meetings.mid}">
                                            <span class="mr-3 avatar-icon bg-success-lighten-2"><i
                                                    class="icon-present"></i></span>
                                                <div class="notification-content">
                                                    <h6 class="notification-heading">会议主题:${meetings.mtitle}</h6>
                                                    <span class="notification-text">开始时间:${meetings.meetingstart}</span>
                                                </div>
                                            </a>
                                        </li>
                                    </c:forEach>
                                </ul>

                            </div>
                        </div>
                    </li>
                    <li class="icons dropdown d-none d-md-flex">
                        <a href="javascript:void(0)" class="log-user" data-toggle="dropdown">
                            <span>当前登录人:${currentUser.employeeVO.ename}</span> <i class="fa fa-angle-down f-s-14"
                                                                                  aria-hidden="true"></i>
                        </a>
                        <div class="drop-down dropdown-language animated fadeIn  dropdown-menu">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="javascript:void()">所属部门:${currentUser.employeeVO.dept.dname}</a></li>
                                    <li><a href="javascript:void()">职位:${currentUser.employeeVO.role.rname}</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="icons dropdown">
                        <div class="user-img c-pointer position-relative" data-toggle="dropdown"
                             style="z-index: 999;position: absolute">
                            <span class="activity active"></span>
                            <img src="images/user/1.png" height="40" width="40" alt="">
                        </div>
                        <div class="drop-down dropdown-profile   dropdown-menu">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li>
                                        <a href="${bp}/employee/showuser.action"><i class="icon-user"></i> <span>个人信息</span></a>
                                    </li>
                                    <c:if test="${currentUser.employeeVO.dept.did==3}">
                                        <li>
                                            <a href="${bp}/rad/toprojectlist.action?type=2&eid=${currentUser.employeeVO.eid}"><i
                                                    class="icon-user"></i> <span>我的项目</span></a>
                                        </li>
                                    </c:if>

                                    <li>
                                        <a href="${bp}/employee/toleavelist.action?type=2&eid=${currentUser.employeeVO.eid}"><i
                                                class="icon-user"></i> <span>我的请假</span></a>
                                    </li>
                                    <li>
                                        <a href="${bp}/employee/toexpenselist.action?type=2&eid=${currentUser.employeeVO.eid}"><i
                                                class="icon-user"></i> <span>我的报销</span></a>
                                    </li>
                                    <li>
                                        <a href="${bp}/employee/toupdatepass.action"><i class="icon-envelope-open"></i> <span>修改密码</span>
                                        </a>
                                    </li>

                                    <hr class="my-2">
                                    <li>
                                        <a href="${bp}/employee/exit.action"><i class="icon-lock"></i> <span>安全退出</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

</html>