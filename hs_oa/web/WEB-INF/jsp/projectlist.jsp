<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>项目列表</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


</head>

<body>
    <div id="main-wrapper">
        <%@include file="head.jsp"%>

        <%@include file="left_nav.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">项目列表</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">项目管理</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">项目列表</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>项目编号</th>
                                                <th>项目名称</th>
                                                <th>项目负责人</th>
                                                <th>项目立项时间</th>
                                                <th>项目进度</th>
                                                <th>更新</th>
                                                <th>合同</th>
                                                <th>操作</th>
                                                <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                    <th>审批</th>
                                                </c:if>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="project" items="${list}">
                                                <tr>
                                                    <td>${project.pid}</td>
                                                    <td><a href="${bp}/rad/toprojectinfo.action?pid=${project.pid}">${project.pname}</a></td>
                                                    <td><a href="${bp}/rad/toprojectlist.action?type=2&eid=${project.employeeVO.eid}"> ${project.employeeVO.ename}</a></td>
                                                    <td><fmt:formatDate value="${project.pcreatetime}" pattern="yyyy-MM-dd hh:mm"></fmt:formatDate></td>
                                                    <td>
                                                        <c:if test="${project.approvestate.asid==2}">
                                                           <span class="float-right">${project.pprogress.ppnum}%</span>
                                                            <div class="progress" style="height: 9px">
                                                                <div class="progress-bar bg-info wow  progress-" style="width: ${project.pprogress.ppnum}%;" role="progressbar"><span class="sr-only">${project.pprogress.ppnum}%</span>
                                                                </div>
                                                            </div>
                                                        </c:if>
                                                        <c:if test="${project.approvestate.asid!=2}">
                                                            暂无进度
                                                        </c:if>
                                                    </td>
                                                    <td>
                                                        <c:if test="${project.approvestate.asid==2}">
                                                            <c:if test="${project.employeeVO.eid==currentUser.employeeVO.eid}">
                                                                <span><a href="${bp}/rad/toeditprogress.action?pid=${project.pid}"class="label gradient-1 btn-rounded">更新进度</a></span>
                                                            </c:if>
                                                        </c:if>
                                                    </td>
                                                    <td>
                                                        <c:if test="${project.approvestate.asid==2}">
                                                            <a class="loginpage" href="${bp}/rad/topcontractlist.action?pid=${project.pid}&type=2">查看合同</a>
                                                        </c:if>
                                                        <c:if test="${project.approvestate.asid!=2}">
                                                            暂无合同
                                                        </c:if>
                                                    </td>
                                                    <td>

                                                        <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                            <c:if test="${project.approvestate.asid!=3}">
                                                                <button class="btn btn-primary" type="button" onclick="window.location.href='${bp}/rad/toupdateproject.action?pid=${project.pid}'">更换负责人</button>
                                                            </c:if>
                                                            <c:if test="${project.approvestate.asid==3}">
                                                                无法更换
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${currentUser.employeeVO.role.rid!=3}">
                                                            暂无权限
                                                        </c:if>
                                                    </td>
                                                    <td>
                                                        <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                            <c:if test="${project.approvestate.asid==1}">
                                                                <button class="btn btn-facebook" type="button" onclick="window.location.href='${bp}/rad/toprojectinfo.action?pid=${project.pid}'"> 审批</button></td>
                                                            </c:if>
                                                            <c:if test="${project.approvestate.asid==2}">已通过</c:if>
                                                            <c:if test="${project.approvestate.asid==3}">已拒绝</c:if>
                                                        </c:if>
                                                        <c:if test="${currentUser.employeeVO.role.rid!=3}">
                                                            <c:if test="${project.approvestate.asid==1}">待审核</c:if>
                                                            <c:if test="${project.approvestate.asid==2}">已通过</c:if>
                                                            <c:if test="${project.approvestate.asid==3}">已拒绝</c:if>
                                                        </c:if>
                                                    </td>

                                                </tr>
                                            </c:forEach>
                                        </tbody>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
    <script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>

</body>

</html>