<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>请假表</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
            引入头部导航
        ***********************************-->
    <%@include file="head.jsp" %>

    <!--**********************************
        引入外部左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp" %>
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">请假列表</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">请假管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">请假列表</h4>
                            <div class="table-responsive">
                                <form id="form1">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                        <tr>
                                            <th>请假编号</th>
                                            <th>请假员工姓名</th>
                                            <th>请假原因</th>
                                            <th>请假开始时间</th>
                                            <th>请假结束时间</th>
                                            <th>请假类型</th>
                                            <th>是否销假</th>
                                            <th>操作</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${list}" var="leave">
                                            <tr>
                                                <td><a href='${bp}/employee/leaveinfo.action?lid=${leave.lid}'>${leave.lid}</a></td>
                                                    <%--请假编号--%>
                                                <td><a href='${bp}/employee/toleavelist.action?type=2&eid=${currentUser.employeeVO.eid}'>${leave.employeeVO.ename}</a></td>
                                                    <%-- 请假员工姓名--%>
                                                <td><a href='${bp}/employee/leaveinfo.action?lid=${leave.lid}'>${leave.reason}</a></td>
                                                <td>${leave.start}</td>

                                                    <%-- 请假开始时间--%>
                                                <td>${leave.end}</td>
                                                    <%--请假结束时间--%>
                                                <td>${leave.leavetype.ltype}</td>
                                                    <%--请假类型编号--%>

                                                <td>
                                                <c:if test="${leave.lstate.equals('未销假')}">
                                                    <span class="label gradient-2 rounded">${leave.lstate}</span>
                                                    <c:if test="${currentUser.employeeVO.eid==leave.employeeVO.eid}">
                                                        <c:if test="${leave.approvestate.asid==2}">
                                                             <span class="label gradient-8 rounded" style="cursor: pointer"
                                                                   onclick="window.location.href='${bp}/employee/editleave.action?lid=${leave.lid}'">
                                                            销假
                                                    </span>
                                                        </c:if>
                                                    </c:if>
                                                </c:if>
                                                    <c:if test="${leave.lstate.equals('已销假')}">
                                                        <span class="label gradient-8 rounded">${leave.lstate}</span>
                                                    </c:if>
                                                </td>
                                                <td>
                                                    <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                        <c:if test="${leave.approvestate.asid==1}">
                                                        <span style="cursor: pointer"  onclick="window.location.href='${bp}/employee/leaveinfo.action?lid=${leave.lid}'" class="label gradient-7 rounded">
                                                            去审批
                                                            </span>
                                                            &nbsp;
                                                            <c:if test="${currentUser.employeeVO.eid==leave.employeeVO.eid}">
                                                                <span style="cursor: pointer" onclick="window.location.href='${bp}/employee/deleteleave.action?lid=${leave.lid}'" class="label gradient-3 rounded">去取消</span>
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${leave.approvestate.asid==2}"><span class="label gradient-4 rounded">已通过</span></c:if>
                                                        <c:if test="${leave.approvestate.asid==3}"><span class="label gradient-2-shadow rounded">已拒绝</span></c:if>
                                                    </c:if>
                                                    <c:if test="${currentUser.employeeVO.role.rid!=3}">
                                                        <c:if test="${leave.approvestate.asid==1}">
                                                            <span class="label gradient-1 rounded">待审核</span>
                                                            <c:if test="${currentUser.employeeVO.eid==leave.employeeVO.eid}">
                                                                <span style="cursor: pointer" onclick="window.location.href='${bp}/employee/deleteleave.action?lid=${leave.lid}'" class="label gradient-3 rounded">去取消</span>
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${leave.approvestate.asid==2}"><span class="label gradient-4 rounded">已通过</span></c:if>
                                                        <c:if test="${leave.approvestate.asid==3}"><span class="label gradient-2-shadow rounded">已拒绝</span></c:if>
                                                </c:if>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->


    <!--**********************************
        Footer start
    ***********************************-->

    <!--**********************************
        Footer end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
<script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>

</body>
</html>