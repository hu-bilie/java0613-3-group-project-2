<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>更新劳动合同</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <%@include file="head.jsp" %>
    <!--**********************************
        Header end ti-comment-alt
    ***********************************-->

    <!--**********************************
        Sidebar start
    ***********************************-->
    <%@include file="left_nav.jsp" %>
    <!--**********************************
        Sidebar end
    ***********************************-->

    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">劳动合同修改</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">劳动合同管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-validation">
                                <form class="form-valide" action="${bp}/personnel/editlc.action" method="post"
                                      enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">合同编号 <span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="lcid" name="lcid"
                                                   readonly="readonly" value="${laborContract.lcid}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">员工姓名 <span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                                <input type="text" class="form-control" id="ename" name="eid" readonly="readonly"
                                                       value="${laborContract.ename}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">劳动合同开始时间<span
                                                class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="lcstart" name="lcstart"
                                                   readonly="readonly" value="${laborContract.lcstart}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">劳动合同结束时间<span
                                                class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="lcstop" name="lcstop"
                                                   readonly="readonly" value="${laborContract.lcstop}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">合同状态 <span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <c:if test="${laborContract.lcsid==1}">
                                                <label>
                                                    <input type="radio"name="lcsid" checked value="1"><span>存续</span>
                                                </label>
                                                <label>
                                                    <input type="radio"name="lcsid"  value="2"><span>终止</span>
                                                </label>
                                                <label>
                                                    <input type="radio" name="lcsid"value="3"><span>解除</span>
                                                </label>
                                            </c:if>
                                            <c:if test="${laborContract.lcsid==2}">
                                                <input type="hidden" name="lcsid"value="2">
                                                <h4>已终止，不可修改</h4>
                                            </c:if>
                                            <c:if test="${laborContract.lcsid==3}">
                                                <input type="hidden" name="lcsid"value="3">
                                                <h4>已解除，不可修改</h4>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="submit" class="btn btn-success btn-rounded">提交</button>
                                            <button type="button" class="btn btn-primary btn-rounded"
                                                    onclick="window.history.go(-1)">返回
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->

</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/validation/jquery.validate.min.js"></script>
<script src="./plugins/validation/jquery.validate-init.js"></script>

</body>

</html>