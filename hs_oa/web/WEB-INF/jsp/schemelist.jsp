<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>项目方案列表</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            引入头部导航
        ***********************************-->
        <%@include file="head.jsp"%>

        <!--**********************************
            引入外部左侧导航
        ***********************************-->
        <%@include file="left_nav.jsp"%>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <button class="btn mb-1 btn-primary btn-lg" onclick="window.location.href='${bp}/personnel/toaddrole.action'">
                        新增方案
                    </button>
                </div>
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">方案管理</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">方案列表</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">方案信息</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>方案编号</th>
                                                <th>方案名称</th>
                                                <th>方案内容</th>
                                                <th>项目名称</th>
                                                <th>操作</th>

                                            </tr>
                                        </thead>
                                        <tbody>
<%--          int eid, String ename, String esex, Date birthday, String tel, String address, Role role, Dept dept, Estate estate, Date entrydate--%>
                                        <c:forEach items="${list}" var="scheme">
                                            <tr>
                                                <td>${scheme.scid}</td>
                                                <td>${scheme.sctitle}</td>
                                                <td>${scheme.scontent}</td>
                                                <td>${scheme.pname}</td>
                                                <td><a href="${bp}/rad/toeditscheme.action?scid=${scheme.scid}">修改</a></td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
    </div>
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
    <script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>

</body>

</html>