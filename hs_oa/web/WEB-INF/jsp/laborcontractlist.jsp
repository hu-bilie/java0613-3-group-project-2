<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>项目合同列表</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


</head>

<body>
<div id="main-wrapper">
    <%@include file="head.jsp"%>

    <%@include file="left_nav.jsp"%>
    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">劳动合同合同列表</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">合同管理</a></li>
                </ol>
            </div>
        </div>
        <c:if test="${state==2}">
            <div class="col-12 text-left" style="margin-left:40px;" >
                <button class="btn btn-danger px-5" onclick="window.location.href='${bp}/personnel/toaddlc.action?eid=${employee.eid}'">上传新的合同</button>
            </div>
        </c:if>

        <!-- row -->
        <div class="container-fluid">
            <c:forEach var="laborContractVO" items="${list}">
                <div class="card-body" style="margin:20px;padding：40px; width: 450px;background-color:#fff;float: left">
                    <div class="media align-items-center mb-4">
                        <div class="media-body">
                            <h4 class="mb-0">员工合同</h4>
                            <p class="text-muted mb-0">合同编号：${laborContractVO.lcid}</p>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col">
                            <div class="card card-profile text-center">
                                <h3 class="mb-0">签约员工：${laborContractVO.ename}</h3>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button class="btn btn-danger px-5" onclick="window.location.href='${laborContractVO.lcaddr}'">预览合同</button>
                            <button class="btn btn-info px-5" onclick="window.location.href='${bp}/personnel/toeditlc.action?lcid=${laborContractVO.lcid}'">修改合同</button>
                        </div>
                    </div>
                    <h4>关于合同：</h4>
                    <p></p>
                    <ul class="card-profile__info">
                        <li class="mb-1"><strong class="text-dark mr-4">合同生效时间</strong> <span>${laborContractVO.lcstart}</span></li>
                        <li><strong class="text-dark mr-4">合同截止时间</strong> <span>${laborContractVO.lcstop}</span></li>
                        <li><strong class="text-dark mr-4">当前状态</strong> <span class="label gradient-1 rounded">${laborContractVO.lcstate}</span></li>
                    </ul>
                </div>
            </c:forEach>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
<script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>

</body>

</html>