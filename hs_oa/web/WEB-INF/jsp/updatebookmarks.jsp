<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>修改收藏夹</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
       引入头部导航
    ***********************************-->
    <%@include file="head.jsp"%>
    <!--**********************************
        引入左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp"%>

    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">修改收藏夹</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">收藏管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-validation">
                                <form id="form1" class="form-valide" action="${bp}/employee/updatebookmarks.action?bmid=${bookmarks.bmid}" method="post">
                                    <input type="hidden" name="aid" value="${currentUser.aid}">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="val-username">原名称
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="val-username" value="${bookmarks.bmname}" readonly="readonly">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">新名称<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input id="bmname" type="text" class="form-control" placeholder="请输入该收藏夹的新名称" required="required" name="bmname"></input>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="button" onclick="myfun()" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/validation/jquery.validate.min.js"></script>
<script src="./plugins/validation/jquery.validate-init.js"></script>


<script>
    function myfun(){
        var bmname=$("#bmname").val().trim();
        if(bmname==""){
            alert("输入名称不能少于一个字");
            return false;
        }
        $("#form1").submit();
    }
</script>
</body>

</html>
