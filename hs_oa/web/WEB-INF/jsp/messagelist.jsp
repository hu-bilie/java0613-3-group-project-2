<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>客户表</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">


    <!--**********************************
            引入头部导航
        ***********************************-->
    <%@include file="head.jsp"%>

    <!--**********************************
        引入外部左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp"%>



    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">客户列表</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">客户管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row">

                <div class="col-12">
                    <div class="card">

                        <div class="card-body">

                            <h4 class="card-title">客户列表</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                    <tr>
                                        <th>客户编号</th>
                                        <th>客户名称</th>
                                        <th>客户电话</th>
                                        <th>客户邮箱</th>
                                        <th>留言概要</th>
                                        <th>留言详情</th>
                                        <th>留言时间</th>
                                        <th>反馈状态</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${list}" var="message">
                                        <tr>
                                            <td>${message.id}</td>
                                            <td><a href="${bp}/leader/messageinfo.action?id=${message.id}">${message.name}</a></td>
                                            <td>${message.tel}</td>
                                            <td>${message.email}</td>
                                            <td>${message.subject}</td>
                                            <td>${message.message}</td>
                                            <td>${message.submittime}</td>
                                            <td>${message.feedback}
                                                   <c:if test="${message.feedback.equals('未反馈')}">
                                                       <span style="cursor: pointer" class="label gradient-2" onclick="window.location.href='${bp}/leader/messageinfo.action?id=${message.id}'">去反馈</span>
                                                   </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->


    <!--**********************************
        Footer start
    ***********************************-->

    <!--**********************************
        Footer end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
<script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>
<script>
    function checkexid(asid,exid){
        console.log(asid);
        console.log(exid);
        $.post("/hs_oa/checkexid.action",{"asid":asid,"exid":exid},function (data){
            console.log(data);
         if (data){
          alert("审核成功!");
         }else{
          alert("审核失败!");
         }
        });
    }
</script>

</body>


</html>