<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>报销列表</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">


    <!--**********************************
            引入头部导航
        ***********************************-->
    <%@include file="head.jsp"%>

    <!--**********************************
        引入外部左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp"%>



    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">报销列表</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">报销列表</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row">

                <div class="col-12">
                    <div class="card">

                        <div class="card-body">

                            <h4 class="card-title">报销列表</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                    <tr>
                                        <th>报销编号</th>
                                        <th>报销金额</th>
                                        <th>员工姓名</th>
                                        <th>审批</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${list}" var="expenseVO">
                                        <tr>
                                            <td>${expenseVO.exid}</td>
                                            <td>${expenseVO.money}</td>
                                            <td>${expenseVO.employeeVO.ename}</td>
                                            <td>
                                                <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                    <c:if test="${expenseVO.approveState.asid==1}">
                                                        <span style="cursor: pointer"  onclick="window.location.href='${bp}/employee/expenseinfo.action?exid=${expenseVO.exid}'" class="label gradient-7 rounded">
                                                            去审批
                                                            </span>
                                                        &nbsp;
                                                        <c:if test="${currentUser.employeeVO.eid==expenseVO.employeeVO.eid}">
                                                            <span style="cursor: pointer" onclick="window.location.href='${bp}/employee/deleteexpense.action?exid=${expenseVO.exid}'" class="label gradient-3 rounded">去取消</span>
                                                        </c:if>
                                                    </c:if>
                                                    <c:if test="${expenseVO.approveState.asid==2}"><span class="label gradient-4 rounded">已通过</span></c:if>
                                                    <c:if test="${expenseVO.approveState.asid==3}"><span class="label gradient-2-shadow rounded">已拒绝</span></c:if>
                                                </c:if>
                                                <c:if test="${currentUser.employeeVO.role.rid!=3}">
                                                    <c:if test="${expenseVO.approveState.asid==1}">
                                                        <span class="label gradient-1 rounded">待审核</span>
                                                        <c:if test="${currentUser.employeeVO.eid==expenseVO.employeeVO.eid}">
                                                            <span style="cursor: pointer" onclick="window.location.href='${bp}/employee/deleteexpense.action?exid=${expenseVO.exid}'" class="label gradient-3 rounded">去取消</span>
                                                        </c:if>
                                                    </c:if>
                                                    <c:if test="${expenseVO.approveState.asid==2}"><span class="label gradient-4 rounded">已通过</span></c:if>
                                                    <c:if test="${expenseVO.approveState.asid==3}"><span class="label gradient-2-shadow rounded">已拒绝</span></c:if>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->


    <!--**********************************
        Footer start
    ***********************************-->

    <!--**********************************
        Footer end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
<script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>


</body>


</html>