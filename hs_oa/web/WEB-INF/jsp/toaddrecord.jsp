<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>会议列表</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


</head>

<body>
<div id="main-wrapper">
    <%@include file="head.jsp" %>

    <%@include file="left_nav.jsp" %>
    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">会议列表</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">会议管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">会议列表</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                    <tr>
                                        <th>会议编号</th>
                                        <th>会议主题</th>
                                        <th>所属部门</th>
                                        <th>会议开始时间</th>
                                        <th>会议结束时间</th>
                                        <th>会议状态</th>
                                        <th>会议记录</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="meeting" items="${list}">
                                        <c:if test="${currentUser.employeeVO.dept.did==meeting.dept.did||currentUser.employeeVO.role.rid==3}">
                                            <tr>
                                                <td>${meeting.mid}</td>
                                                <td>${meeting.mtitle}</td>
                                                <td>${meeting.dept.dname}</td>
                                                <td>${meeting.meetingstart}</td>
                                                <td>${meeting.meetingstop}</td>
                                                <td>${meeting.meetingstate}</td>
                                                <td><span style="cursor: pointer"
                                                          onclick="window.location.href='${bp}/employee/addrecord.action?mid=${meeting.mid}'"
                                                          class="label gradient-7 rounded">添加</span></td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
<script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>

</body>

</html>