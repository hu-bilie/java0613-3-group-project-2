<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<base href="${bp}/statics/">
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>修改项目合同</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<%@include file="head.jsp"%>

<%@include file="left_nav.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">修改项目合同</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">项目</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <form id="form1" action="${bp}/rad/editpcontract.action" method="post">
                                <input name="type" type="hidden"value="1"  readonly>
                                <div class="card-body">
                                    <input type="hidden" value="${pcontractVO.id}"name="id" id="id">

                                    <h4 class="card-title">项目合同编号</h4>
                                    <p class="text-muted m-b-15 f-s-12">合同编号必须唯一！ <code>请注意大小写</code> </p>
                                    <div class="form-group">
                                        <input type="text" readonly onblur="selectPcid(this.value)" value="${pcontractVO.pcid}" class="form-control input-default" id="pcid" name="pcid">
                                    </div>
                                    <p id="mess" style="color: red" >${mess}</p>
                                    <div class="form-group">
                                        <h4><a href="${pcontractVO.pcaddr}">下载查看原合同</a></h4>
                                    </div>
                                    <c:if test="${pcontractVO.pcstate.pcsid==4||pcontractVO.pcstate.pcsid==3}">
                                        <h4 class="card-title">合作伙伴</h4>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-default" readonly name="partner"value="${pcontractVO.partner}">
                                        </div>
                                        <h4 class="card-title">项目编号</h4>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control input-default"  name="pid" value="${pcontractVO.projectVO.pid}">
                                        </div>
                                        <h4 class="card-title">项目名称</h4>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control input-default"  name="pname" value="${pcontractVO.projectVO.pname}">
                                        </div>
                                        <div class="example">
                                            <h4 class="box-title m-t-30">合同有效期限</h4>
                                            <div class="input-daterange input-group" >
                                                <input value="${pcontractVO.pcstart}" type="datetime-local" class="form-control input-default" readonly name="pcstart1"> <span class="input-group-addon bg-info b-0 text-white">to</span>
                                                <input value="${pcontractVO.pcstop}" type="datetime-local" class="form-control input-default"  readonly name="pcstop2">
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test="${pcontractVO.pcstate.pcsid==1||pcontractVO.pcstate.pcsid==2}">
                                    <h4 class="card-title">合作伙伴</h4>
                                    <div class="form-group">
                                        <input type="text" class="form-control input-default" id="partner" name="partner"value="${pcontractVO.partner}">
                                    </div>
                                    <h4 class="card-title">项目编号</h4>
                                    <div class="form-group">
                                        <input type="text" readonly class="form-control input-default" id="pid" name="pid" value="${pcontractVO.projectVO.pid}">
                                    </div>
                                    <h4 class="card-title">项目名称</h4>
                                    <div class="form-group">
                                        <input type="text" readonly class="form-control input-default" id="pname" name="pname" value="${pcontractVO.projectVO.pname}">
                                    </div>
                                    <div class="example">
                                        <h4 class="box-title m-t-30">合同有效期限</h4>
                                        <div class="input-daterange input-group" id="date-range">
                                            <input value="${pcontractVO.pcstart}" type="datetime-local" class="form-control input-default" id="pcstart1"name="pcstart1"> <span class="input-group-addon bg-info b-0 text-white">to</span>
                                            <input value="${pcontractVO.pcstop}" type="datetime-local" class="form-control input-default" id="pcstop2"name="pcstop2">
                                        </div>
                                    </div>
                                    </c:if>
                                    <br>
                                    <div class="form-group">
                                        <div class="radio mb-3">
                                             <c:if test="${pcontractVO.pcstate.pcsid==1}">
                                            <label>
                                                <input type="radio"name="pcstateid" checked value="1"><span>未履约</span>
                                            </label>
                                            <label>
                                                <input type="radio"name="pcstateid" value="2"><span>已履约</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="pcstateid"value="4"><span>已解约</span>
                                            </label>
                                        </c:if>
                                        <c:if test="${pcontractVO.pcstate.pcsid==2}">
                                            <label>
                                                <input type="radio"name="pcstateid"  value="1"><span>未履约</span>
                                            </label>
                                            <label>
                                                <input type="radio"name="pcstateid" checked value="2"><span>已履约</span>
                                            </label>
                                            <label>
                                                <input type="radio" name="pcstateid"value="4"><span>已解约</span>
                                            </label>
                                        </c:if>
                                            <c:if test="${pcontractVO.pcstate.pcsid==3}">
                                                <input type="hidden" name="pcstateid"value="3">
                                                <h4>已过期，不可修改</h4>
                                            </c:if>
                                        <c:if test="${pcontractVO.pcstate.pcsid==4}">
                                            <input type="hidden" name="pcstateid"value="3">
                                            <h4>已解约，不可修改</h4>
                                        </c:if>
                                    </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-dark" onclick="submit1()" type="button">提交</button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button class="btn btn-outline-danger" onclick="window.history.back(-1)">放弃修改</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
</body>
<script>
    function  selectPcid(pcid){
        console.log(pcid);
        var id=$("#id").val();
        $.post("${bp}/selectpcid1.action",{"pcid":pcid,"id":id},function (resp){
            console.log(resp);
            if(resp){
                $("#mess").html("")
            }else {
                $("#mess").html("此合同编号已存在！")
            }
        })
    }
    function submit1(){
        console.log("进入表单提交验证。。。")
        if ($("#pcid").val().trim()==""){
            alert("合同编号不能为空！")
            return ;
        }
        if ($("#partner").val()==""){
            alert("请输入合作公司名称！");
            return ;
        }
        var pcstart = $("#pcstart1").val();
        var pcstop = $("#pcstop2").val();
        if (pcstop<=pcstart){
            alert("有效期不合理,请重新选择");
            return false;
        }
        $("#form1").submit();
    }
</script>
</html>