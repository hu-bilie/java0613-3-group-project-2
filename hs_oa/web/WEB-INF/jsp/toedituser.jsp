<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>修改员工</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
        Nav header start
    ***********************************-->
    <!--**********************************
        Nav header end
    ***********************************-->

    <!--**********************************
       标题开始
   ***********************************-->
    <%@include file="head.jsp" %>
    <!--**********************************
        Header end ti-comment-alt
    ***********************************-->

    <!--**********************************
        导航栏开始
    ***********************************-->
    <%@include file="left_nav.jsp" %>
    <!--**********************************
        导航栏结束
    ***********************************-->

    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">修改资料</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">员工管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-validation">
                                <form name="form1" id="form1" class="form-valide" action="${bp}/employee/edituser.action" method="post">
                                    <input type="hidden" id="eid" name="eid" value="${employee.eid}">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">姓名:<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="ename" name="ename"
                                                   value="${employee.ename}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">性别:<span class="text-danger">*</span>
                                        </label>
                                        <label class="radio-inline mr-3">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="sex" value="男"
                                                                                  checked>男
                                        </label>
                                        <label class="radio-inline mr-3">
                                            <input type="radio" name="sex" value="女">女
                                        </label>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">出生日期<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="date" name="birthday" class="form-control"
                                                   value="${employee.birthday}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="val-confirm-password">电话<span
                                                class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="val-confirm-password"
                                                   name="tel" value="${employee.tel}" onblur="checkTel(this.value)">
                                            <p id="mess" class="warning" style="color: red"></p>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">地址<span
                                                class="text-danger">*</span>
                                        </label>

                                        <div class="col-lg-6">

                                            <select  class="form-control address col-md-4"
                                                    style="float: left;width: 400px;"
                                                    id="provincelist" name="provinceId"
                                                    onchange="cityload(this.value)">
                                                <option value='0'>--省--</option>
                                            </select>
                                            <select class="form-control address col-md-4" id="citylist" name="cityId"
                                                    style="float: left;width: 400px;"
                                                    onchange="areaload(this.value)">
                                                <option value='0'>--市--</option>
                                            </select>
                                            <select class="form-control address col-md-4" id="arealist" name="areaId"
                                                    style="float: left;width: 400px;">
                                                <option value='0'>--区--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <c:if test="${currentUser.employeeVO.dept.did==5||currentUser.employeeVO.role.rid==3}">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">角色:<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="rid" name="rid">
                                                    <c:forEach var="role" items="${rlist}">
                                                        <option value='${role.rid}'>${role.rname}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">部门:<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="did" name="did">
                                                    <c:forEach var="dept" items="${dlist}">
                                                        <option value='${dept.did}'>${dept.dname}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test="${currentUser.employeeVO.dept.did!=5&&currentUser.employeeVO.role.rid!=3}">
                                        <input readonly type="hidden" name="rid" value="${employee.role.rid}">
                                        <input readonly type="hidden" name="did" value="${employee.dept.did}">
                                    </c:if>
                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="button" onclick="myfun()" class="btn btn-primary">保存</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->


    <!--**********************************
        Footer start
    ***********************************-->

    <!--**********************************
        Footer end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/validation/jquery.validate.min.js"></script>
<script src="./plugins/validation/jquery.validate-init.js"></script>

<script type="text/javascript" src="${bp}/statics/js/jquery.min.js"></script>
<script type="text/javascript" src="${bp}/statics/js/bootstrap.js"></script>
<script type="text/javascript" src="${bp}/statics/js/perfect-scrollbar.min.js"></script>
<!--标签插件-->
<script src="${bp}/statics/js/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${bp}/statics/js/main.min.js"></script>

<script>
    $(function () {
        provinceload();
    })

    function provinceload() {
        $.post("${bp}/provincelist.action", {}, function (resp) {
            console.log(resp);
            var html = "<option value='0'>--省--</option>"
            $.each(resp, function (i, province) {
                console.log(province.provinceId);
                console.log(province.province);
                html += "<option value='" + province.provinceId + "'>" + province.province + "</option>";
            })
            console.log(html);
            $("#provincelist").html(html);
        })
    }

    function cityload(provinceId) {
        console.log(provinceId);
        $.post("${bp}/citylist.action", {"provinceId": provinceId}, function (resp) {
            console.log(resp);
            var html = "<option value='0'>--市--</option>"
            $.each(resp, function (i, city) {
                console.log(city.cityId);
                console.log(city.city);
                html += "<option value='" + city.cityId + "'>" + city.city + "</option>";
            })
            console.log(html);
            $("#citylist").html(html);
            areaload(0)
        })

    }

    function areaload(cityId) {
        console.log(cityId);
        $.post("${bp}/arealist.action", {"cityId": cityId}, function (resp) {
            console.log(resp);
            var html = "<option value='0'>--区--</option>"
            $.each(resp, function (i, area) {
                console.log(area.areaId);
                console.log(area.areas);
                html += "<option value='" + area.areaId + "'>" + area.areas + "</option>";
            })
            console.log(html);
            $("#arealist").html(html);

        })
    }

    function checkTel(phone){
        console.log(phone);
        $.post("${bp}/checktel.action",{"tel":phone},function (data){
            console.log(data);
            if(data==false){
                $("#mess").html("该电话已注册");
            }else{
                $("#mess").html("");
            }
        });
    }

    function myfun(){
        var phoneNumReg = /^1[3|4|5|7|8]\d{9}$/;
        if (!phoneNumReg.test(document.form1.tel.value)){
            alert("请正确输入手机号");
            return false;
        }

        $("#form1").submit();
    }

</script>


</body>

</html>