<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>添加劳动合同</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="kindeditor/themes/default/default.css" />
</head>

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="index.html">
                    <b class="logo-abbr"><img src="images/logo.png" alt=""> </b>
                    <span class="logo-compact"><img src="./images/logo-compact.png" alt=""></span>
                    <span class="brand-title">
                        <img src="images/logo-text.png" alt="">
                    </span>
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <%@include file="head.jsp"%>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <%@include file="left_nav.jsp"%>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">上传劳动合同</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">劳动合同管理</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form id="form1" class="form-valide" action="${bp}/personnel/addlc.action" method="post" enctype="multipart/form-data">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="lcid">合同编号 <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" onblur="selectlcid(this.value)" class="form-control" id="lcid" name="lcid" placeholder="请输入合同编号" required="required">
                                                <p class="text-muted m-b-15 f-s-12">合同编号必须唯一！ <code>请注意大小写</code> </p>
                                                <p id="mess" style="color: red" >${mess}</p>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">员工姓名 <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input readonly class="form-control" type="text" name="ename" value="${employee.ename}">
                                                <input readonly  type="hidden" name="eid" value="${employee.eid}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">劳动合同开始时间<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-md-6">
                                                <input type="datetime-local" name="lcstart1" id="lcstart" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">劳动合同结束时间<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-md-6">
                                                <input type="datetime-local" name="lcstop1" id="lcstop" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">上传合同扫描件<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-md-6">
                                            <form action="upload.action" method="post" enctype="multipart/form-data">
                                                <input type="file" name="file" id="file">
                                            </form>
                                            </div>
                                        </div>
                                        <p class="text-muted m-b-15 f-s-12">合同格式为pdf格式！<span id="mess2" style="color: red" >${mess2}</span></p>
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="button" class="btn btn-success btn-rounded" onclick="submit1()">提交</button>
                                                <button type="button" class="btn btn-primary btn-rounded" onclick="window.history.go(-1)">返回</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <script src="./plugins/validation/jquery.validate.min.js"></script>
    <script src="./plugins/validation/jquery.validate-init.js"></script>
    <script>
        function  selectlcid(lcid){
            console.log(lcid);
            $.post("${bp}/selectlcid.action",{"lcid":lcid},function (resp){
                console.log(resp);
                if(resp){
                    $("#mess").html("")
                }else {
                    $("#mess").html("此合同编号已存在！")
                }
            })
        }
        function submit1(){
            console.log("进入表单提交验证。。。")
            if ($("#lcid").val().trim()==""){
                alert("合同编号不能为空！")
                return ;
            }
            if ($("#file").val().trim()==""){
                alert("请上传合同扫描件！");
                return ;
            }
            var lcstart = $("#lcstart").val();
            var lcstop = $("#lcstop").val();
            if (lcstop<=lcstart){
                alert("合同时间不合理,请重新选择");
                return false;
            }
            if ($("#mess").html()!=""){
                alert("此合同编号已存在！")
                return ;
            }
            $("#form1").submit();
        }
    </script>
</body>

</html>