<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>请假详情</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="kindeditor/themes/default/default.css" />

    <script charset="utf-8" src="kindeditor/kindeditor-min.js"></script>
    <script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>
</head>

<body>

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <%@include file="head.jsp"%>

        <%@include file="left_nav.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">请假详情</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">请假管理</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form method="post" action="${bp}/leader/toapproveleave.action">
                                    <input readonly name="lid" value=" ${leave.lid}" type="hidden">
                                    <div class="form-validation">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >请假编号:</label>
                                            <div class="col-lg-6">
                                                ${leave.lid}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >请假员工姓名：</label>
                                            <div class="col-lg-6">
                                                ${leave.employeeVO.ename}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >请假类型：</label>
                                            <div class="col-lg-6">
                                                ${leave.leavetype.ltype}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >请假原因：</label>
                                            <div class="col-lg-6">
                                                ${leave.reason}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >请假开始时间</label>
                                            <div class="col-lg-6">
                                                <fmt:formatDate value="${leave.start}" pattern="yyyy-MM-dd hh:mm:ss"></fmt:formatDate>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >请假结束时间</label>
                                            <div class="col-lg-6">
                                                <fmt:formatDate value="${leave.start}" pattern="yyyy-MM-dd hh:mm:ss"></fmt:formatDate>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                    <c:if test="${leave.approvestate.asid==1}">
                                                        <button type="submit" name="asid" value="2" class="btn btn-success mb-2 me-2">通过</button>
                                                        <button type="submit" name="asid" value="3" class="btn btn-danger mb-2 me-2">拒绝</button>
                                                    </c:if>
                                                </c:if>
                                                <c:if test="${leave.employeeVO.role.rid!=3}">
                                                </c:if>
                                                <button type="submit" name="asid" value="1" class="btn btn-info mb-2 me-2">返回</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <script src="./plugins/validation/jquery.validate.min.js"></script>
    <script src="./plugins/validation/jquery.validate-init.js"></script>

</body>

</html>