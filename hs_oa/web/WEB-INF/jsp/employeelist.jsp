<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>员工花名册</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
        引入头部导航
    ***********************************-->
    <%@include file="head.jsp" %>

    <!--**********************************
        引入外部左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp" %>

    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">员工花名册</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">员工管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">分配账号</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                    <tr>
                                        <th>工号</th>
                                        <th>姓名</th>
                                        <th>性别</th>
                                        <th>出生日期</th>
                                        <th>电话</th>
                                        <th>籍贯</th>
                                        <th>部门</th>
                                        <th>职位</th>
                                        <th>入职时间</th>
                                        <th>状态</th>
                                        <th>劳务合同</th>
                                        <c:if test="${employeeVO.eid==currentUser.employeeVO.eid||currentUser.employeeVO.role.rid==3||currentUser.employeeVO.dept.did==5}">
                                            <th>编辑</th>
                                        </c:if>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <%--          int eid, String ename, String esex, Date birthday, String tel, String address, Role role, Dept dept, Estate estate, Date entrydate--%>
                                    <c:forEach items="${list}" var="employeeVO">
                                    <tr>
                                        <td>${employeeVO.eid}</td>
                                        <td>${employeeVO.ename}</td>
                                        <td>${employeeVO.esex}</td>
                                        <td>${employeeVO.birthday}</td>
                                        <td>${employeeVO.tel}</td>
                                        <td>${employeeVO.address}</td>
                                        <td>${employeeVO.dept.dname}</td>
                                        <td>${employeeVO.role.rname}</td>
                                        <td>${employeeVO.entrydate}</td>
                                        <td>${employeeVO.estate.estate}
                                            &nbsp;&nbsp;
                                        <span style="cursor: pointer" class="label gradient-2" onclick="window.location.href='${bp}/personnel/toeditestate.action?eid=${employeeVO.eid}'">
                                            修改状态
                                        </span>
                                        </td>
                                        <td><span style="cursor: pointer" class="label gradient-5" onclick="window.location.href='${bp}/personnel/tolaborcontractlist.action?type=2&eid=${employeeVO.eid}'">查看合同</span></td>
                                        <c:if test="${employeeVO.eid==currentUser.employeeVO.eid||currentUser.employeeVO.role.rid==3||currentUser.employeeVO.dept.did==5}">
                                                <td><button class="mdi mdi-pencil" onclick="window.location.href='${bp}/employee/toemployeeinfo.action?eid=${employeeVO.eid}'" title="编辑"></button></td>
                                        </c:if>
                                    </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->


</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
<script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>

</body>

</html>