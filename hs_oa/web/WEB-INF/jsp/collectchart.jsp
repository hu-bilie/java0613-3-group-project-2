<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>内容收藏统计</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body onload="myfun()">

    <!--**********************************
        Header start
    ***********************************-->
    <%@include file="head.jsp" %>
    <!--**********************************
        Header end ti-comment-alt
    ***********************************-->

    <!--**********************************
        Sidebar start
    ***********************************-->
    <%@include file="left_nav.jsp" %>

    <!--**********************************
        Sidebar end
    ***********************************-->

    <!--**********************************
        Content body start
    ***********************************-->
    <div id="main" class="content-body">
        <div class="container-fluid" style="margin-bottom:90px;">
            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">学习内容被收藏统计</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">学习管理</a></li>
                    </ol>
                </div>

            </div>

        </div>
        <div id="main1" style="margin-left: 50px;height: 600px;width: 600px;margin: auto;"></div>


    </div>

    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
    <script type="text/javascript" src="js/echarts.min.js"></script>
    <script>
        function myfun() {
            $.post("http://localhost:8080/hs_oa/personnel/collectchart.action", function (data) {
                var chartDom = document.getElementById("main1");
                console.log(data);
                var myChart = echarts.init(chartDom);
                var option;
                option = {
                    xAxis: {
                        type: 'category',
                        data: data.names
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [
                        {
                            data: data.values,
                            type: 'bar',
                            barWidth:30
                        }
                    ]
                };
                option && myChart.setOption(option);
            });
        }
    </script>

</body>

</html>
