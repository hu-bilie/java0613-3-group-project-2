<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>新增会议</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->
<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
       引入头部导航
    ***********************************-->
    <%@include file="head.jsp" %>
    <!--**********************************
        引入左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp" %>

    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">新增会议</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">会议管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-validation">
                                <form id="form1" class="form-valide" action="${bp}/manager/addmeeting.action" method="post">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="val-username">会议主题<span
                                                class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="val-username" name="mtitle"
                                                   placeholder="请输入会议主题..." required>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">所属部门<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                <select class="form-control" id="sel1" name="did">
                                                    <c:forEach var="dept" items="${list}">
                                                        <option value="${dept.did}">${dept.dname}</option>
                                                    </c:forEach>
                                                </select>
                                            </c:if>
                                            <c:if test="${currentUser.employeeVO.role.rid!=3}">
                                                <input type="text" class="form-control" id="did" name="dname"readonly value="${currentUser.employeeVO.dept.dname}">
                                                <input type="hidden" class="form-control" id="did" name="did"readonly value="${currentUser.employeeVO.dept.did}">
                                            </c:if>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">会议开始时间<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name="meetingstart1" id="meetingstart" type="datetime-local"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">会议结束时间<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name="meetingstop1" id="meetingstop" type="datetime-local" required>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="button" onclick="myfun()" class="btn btn-primary">Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->



</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/validation/jquery.validate.min.js"></script>
<script src="./plugins/validation/jquery.validate-init.js"></script>


<script>

    function myfun() {
        var meetingstart = $("#meetingstart").val();
        var meetingstop = $("#meetingstop").val();
        if (meetingstop <= meetingstart ||new Date(meetingstart).getTime()<new Date().getTime()) {
            alert("会议设置时间不合理,请重新选择");
            return false;
        }
        $("#form1").submit();
    }
</script>
</body>

</html>