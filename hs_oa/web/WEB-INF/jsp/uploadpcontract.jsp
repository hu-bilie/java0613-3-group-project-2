<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>
<base href="${bp}/statics/">
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>项目合同上传</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
<%@include file="head.jsp"%>

<%@include file="left_nav.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">项目合同上传</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">项目管理</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <form id="form1" action="${bp}/rad/uploadpcontract.action" method="post" enctype="multipart/form-data">
                                <div class="card-body">
                                    <h4 class="card-title">项目合同编号</h4>
                                    <p class="text-muted m-b-15 f-s-12">合同编号必须唯一！ <code>请注意大小写</code> </p>
                                    <div class="form-group">
                                        <input type="text" onblur="selectPcid(this.value)" class="form-control input-default" id="pcid" name="pcid" placeholder="Input projectContract id">
                                    </div>
                                    <p id="mess" style="color: red" >${mess}</p>
                                    <h4 class="card-title">上传合同</h4>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="file" name="file" >
                                                <label class="custom-file-label">Choose file</label>
                                            </div>
                                        </div>
                                        <p class="text-muted m-b-15 f-s-12">合同格式为pdf格式！<span id="mess2" style="color: red" >${mess2}</span></p>
                                    </div>
                                    <h4 class="card-title">合作伙伴</h4>
                                    <div class="form-group">
                                        <input type="text" class="form-control input-default" id="partner" name="partner" placeholder="Input our partner">
                                    </div>
                                    <h4 class="card-title">项目编号</h4>
                                    <div class="form-group">
                                        <input type="text" readonly class="form-control input-default" id="pid" name="pid" value="${projectVO.pid}">
                                    </div>
                                    <h4 class="card-title">项目名称</h4>
                                    <div class="form-group">
                                        <input type="text" readonly class="form-control input-default" id="pname" name="pname" value="${projectVO.pname}">
                                    </div>
                                    <div class="example">
                                        <h4 class="box-title m-t-30">合同有效期限</h4>
                                        <div class="input-daterange input-group" id="date-range">
                                            <input type="datetime-local" class="form-control input-default" id="pcstart1"name="pcstart1"> <span class="input-group-addon bg-info b-0 text-white">to</span>
                                            <input type="datetime-local" class="form-control input-default" id="pcstop2"name="pcstop2">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-dark" onclick="submit1()" type="button">提交</button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button class="btn btn-outline-danger" type="reset">重置</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
<script>
    function  selectPcid(pcid){
        console.log(pcid);
        $.post("${bp}/selectpcid.action",{"pcid":pcid},function (resp){
            console.log(resp);
            if(resp){
                $("#mess").html("")
            }else {
                $("#mess").html("此合同编号已存在！")
            }
        })
    }
    function submit1(){
        console.log("进入表单提交验证。。。")
        if ($("#pcid").val().trim()==""){
            alert("合同编号不能为空！")
            return ;
        }
        if ($("#file").val().trim()==""){
            alert("请上传合同扫描件！");
            return ;
        }
        if ($("#partner").val()==""){
            alert("请输入合作公司名称！");
            return ;
        }
        var pcstart = $("#pcstart1").val();
        var pcstop = $("#pcstop2").val();
        if (pcstop<=pcstart){
            alert("有效期不合理,请重新选择");
            return false;
        }
        if ($("#mess").html()!=""){
            alert("此合同编号已存在！")
            return ;
        }
        $("#form1").submit();
    }
</script>
</body>

</html>