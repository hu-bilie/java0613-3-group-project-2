<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head id="head">
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>高效协同,尽在华晟</title>
    <base href="${bp}/qian/">
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Multi - v4.3.0
    * Template URL: https://bootstrapmade.com/multi-responsive-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-between">

        <h1 class="logo" style="font-size: xx-large"><a href="${bp}//index.action"><i>HSOA</i></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="#hero">主页</a></li>
                <li><a class="nav-link scrollto" href="#about">产品介绍</a></li>
                <li><a class="nav-link scrollto" href="#services">功能服务</a></li>
                <li><a class="nav-link scrollto " href="#portfolio">合作伙伴</a></li>
                <li><a class="nav-link scrollto" href="#team">我的团队</a></li>
                <li><a class="nav-link scrollto" href="#certificate">公司认证</a></li>
                <li><a class="nav-link scrollto" href="#contact">联系我们</a></li>
                <li><a class="getstarted scrollto" href="${bp}/tologin.action">登陆体验</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero">
    <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

            <!-- Slide 1 -->
            <div class="carousel-item active" style="background-image: url(assets/img/slide/slide-1.jpg)">
                <div class="carousel-container">
                    <div class="container">
                        <h2 class="animate__animated animate__fadeInDown">高效协同 <span>尽在华晟</span></h2>
                        <p class="animate__animated animate__fadeInUp" style="text-indent:5%">
                            随着经济全球化、信息化时代的到来，传统的管理模式明显不能适应市场经济的高速发展。加快自身管理信息化建设，成为众企业寻求新发展的必然选择，而在企业办公领域，华晟一体化协同办公平台凭借其自身独特的优势已成为企业信息化办公的“必需品”，在信息化大趋势下倍受青睐。</p>
                        <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                    </div>
                </div>
            </div>

            <!-- Slide 2 -->
            <div class="carousel-item" style="background-image: url(assets/img/slide/slide-2.jpg)">
                <div class="carousel-container">
                    <div class="container">
                        <h2 class="animate__animated animate__fadeInDown">效率更高 <span>我们更专业</span></h2>
                        <p class="animate__animated animate__fadeInUp" style="text-indent:5%">
                            在信息化时代，及时的沟通更成为业务协作不可忽视的重要力量。员工之间的沟通越简单、越方便，企业的工作效率越高。华晟让高效成为了企业办公管理的标配。在使得员工从繁琐的工作流程中得到了解放，有效推动了企业信息化发展。</p>
                        <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read
                            More</a>
                    </div>
                </div>
            </div>

            <!-- Slide 3 -->
            <div class="carousel-item" style="background-image: url(assets/img/slide/slide-3.jpg)">
                <div class="carousel-container">
                    <div class="container">
                        <h2 class="animate__animated animate__fadeInDown">更轻松 <span>更自由</span></h2>
                        <p class="animate__animated animate__fadeInUp" style="text-indent:5%">
                            它能够满足你在不同场景下的移动办公和远程协作需求，真正实现企业“无障碍沟通，无边界协作”的理想办公状态。
                            移动办公是当今高速发展的通信业与IT业交融的产物，作为新一代办公模式它不仅改变了我们的生活，更改变了我们的工作方式。</p>
                        <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read
                            More</a>
                    </div>
                </div>
            </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
            <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
            <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
        </a>

    </div>
</section><!-- End Hero -->

<main id="main">

    <!-- ======= 产品介绍 ======= -->
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>About</h2>
                <p>产品介绍</p>
            </div>

            <div class="row content">
                <div class="col-lg-6">
                    <p style="text-indent: 5%">
                        华晟一体化协同办公系统为企业提供的丰富功能应用，帮助企业按需定制所需应用场景，助力企业的数字化转型。
                    </p>
                    <ul>
                        <li><i class="ri-check-double-line"></i>满足各类组织的数字化管理需求</li>
                        <li><i class="ri-check-double-line"></i> 统一办公平台,灵活拓展应用</li>
                        <li><i class="ri-check-double-line"></i> 让组织运营更智能、更协同</li>
                    </ul>
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0">
                    <p style="text-indent: 5%">
                        在现代社会整个办公系统进行实际建设的过程当中，将会有多种不同的管理系统要做好敏捷化的管理，同样也要做好协同化的管理或者是知识化的管理，也要做好一体化的管理，可以通过有效或者是协同的管理方式来积极响应用户的需求，能快速满足所有顾客对一系列产品和服务的需求。
                    </p>
                    <a href="${bp}/tologin.action" class="btn-learn-more">Know More About</a>
                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
        <div class="container" data-aos="fade-up">

            <div class="row no-gutters">

                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <i class="bi bi-emoji-smile"></i>
                        <span data-purecounter-start="0" data-purecounter-end="18" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>年持续获得优秀企业奖</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <i class="bi bi-journal-richtext"></i>
                        <span data-purecounter-start="0" data-purecounter-end="32" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>为大小不同公司提供服务</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <i class="bi bi-headset"></i>
                        <span data-purecounter-start="0" data-purecounter-end="2322" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>次获得合作伙伴好评</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <i class="bi bi-people"></i>
                        <span data-purecounter-start="0" data-purecounter-end="108" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>位公司员工共同成长</p>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Counts Section -->


    <!-- ======= 功能服务 ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Services</h2>
                <p>功能服务</p>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bxl-dribbble"></i></div>
                        <h4>人事管理</h4>
                        <p>Employee increase, employee information management and labor contract management</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in"
                     data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx"></i></div>
                        <h4>项目管理</h4>
                        <p>Project setup, approval and schedule management Project contract management</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in"
                     data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-tachometer"></i></div>
                        <h4>会议管理</h4>
                        <p>Add the function of meeting notification and record, etc</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-world"></i></div>
                        <h4>系统管理</h4>
                        <p>Account distribution, department management, contract approval, etc</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-slideshow"></i></div>
                        <h4>日常工作</h4>
                        <p>Leave management, reimbursement management approval and so on</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-arch"></i></div>
                        <h4>学习提升</h4>
                        <p>Learning content display, new and favorite content</p>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
        <div class="container" data-aos="zoom-in">

            <div class="text-center">
                <h2><p><span>我们希望</span></p></h2>
                <h1><p><span>工作更高效 更轻松 更自由</span></p></h1>
            </div>

        </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>partner</h2>
                <p>合作伙伴</p>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <img src="assets/img/portfolio/3.png" class="img-fluid" style="width: 416px; height: 312px"
                         alt="">
                    <div class="portfolio-info">
                        <h4>云图科技</h4>
                        <p>互联网</p>
                        <a  class="details-link" title="链接一下"><i
                                class="bx bx-link"></i></a>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <img src="assets/img/portfolio/1.png" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4>e家科技</h4>
                        <p>互联网</p>
                        <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                class="bx bx-link"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <img src="assets/img/portfolio/2.png"  class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4>尼蒙网络</h4>
                        <p>互联网</p>
                        <a href="portfolio-details.html" class="details-link" title="More Details"><i
                                class="bx bx-link"></i></a>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Team</h2>
                <p>我的团队</p>
            </div>

            <div class="row">

                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="member" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/team/team-1.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>张三</h4>
                                <span>董事长</span>
                            </div>
                            <div class="social">
                                <a ><i class="bi bi-twitter"></i></a>
                                <a ><i class="bi bi-facebook"></i></a>
                                <a ><i class="bi bi-instagram"></i></a>
                                <a ><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6" data-wow-delay="0.1s">
                    <div class="member" data-aos="zoom-in" data-aos-delay="200">
                        <img src="assets/img/team/team-2.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>沉鱼</h4>
                                <span>执行总裁</span>
                            </div>
                            <div class="social">
                                <a href=""><i class="bi bi-twitter"></i></a>
                                <a href=""><i class="bi bi-facebook"></i></a>
                                <a href=""><i class="bi bi-instagram"></i></a>
                                <a href=""><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6" data-wow-delay="0.2s">
                    <div class="member" data-aos="zoom-in" data-aos-delay="300">
                        <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>王五</h4>
                                <span>财务总监</span>
                            </div>
                            <div class="social">
                                <a href=""><i class="bi bi-twitter"></i></a>
                                <a href=""><i class="bi bi-facebook"></i></a>
                                <a href=""><i class="bi bi-instagram"></i></a>
                                <a href=""><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-6" data-wow-delay="0.3s">
                    <div class="member" data-aos="zoom-in" data-aos-delay="400">
                        <img src="assets/img/team/team-4.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>落雁</h4>
                                <span>研发总监</span>
                            </div>
                            <div class="social">
                                <a href=""><i class="bi bi-twitter"></i></a>
                                <a href=""><i class="bi bi-facebook"></i></a>
                                <a href=""><i class="bi bi-instagram"></i></a>
                                <a href=""><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Team Section -->

    <!-- ======= Pricing Section ======= -->
    <section id="certificate" class="certificate">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>certificate</h2>
                <p>公司认证</p>
            </div>

            <div class="row align-items-center">

                <div class="col-lg-4">
                    <div class="box" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/portfolio/5.jpg" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="box" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/portfolio/6.png" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="box" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/portfolio/7.png" class="img-fluid" alt="">
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Pricing Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Contact</h2>
                <p>联系我们</p>
            </div>

            <div class="row">

                <div class="col-lg-6">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="info-box">
                                <i class="bx bx-map"></i>
                                <h3>Our Address</h3>
                                <p>陕西省西安市碑林区长安北路8号</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box mt-4" style="height: 230px">
                                <i class="bx bx-envelope"></i>
                                <h3>Email Us</h3>
                                <p>0613@zrzy.com</p>
                                <p>hubilie@zrzy.com</p>
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="info-box mt-4" style="height: 230px">
                                <i class="bx bx-phone-call"></i>
                                <h3>Call Us</h3>
                                <p>+1 5589 55488 55<br>+1 6678 254445 41</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <form action="${bp}/getmessage.action" method="post" name="form" id="form" class="php-email-form">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name"
                                       required>
                            </div>
                            <div class="col-md-6 form-group mt-3 mt-md-0">
                                <input type="email" class="form-control" name="email" id="email"
                                       placeholder="Your Email" required>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" name="tel" id="tel" placeholder="tel"
                                   required>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject"
                                   required>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" id="message" name="message" rows="5" placeholder="Message"
                                      required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center">
                            <button type="button" onclick="myfun()">Send Message</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="footer-info">
                        <h3>华晟</h3>
                        <h4><p class="pb-3" style="font-size: large"><em>努力用心 为您服务</em></p></h4>
                        <p>
                            陕西省西安市碑林区<br>
                            长安北路8号<br><br>
                            <strong>联系电话:</strong> +1 5589 55488 55<br>
                            <strong>Email:</strong> hubilie@zrzy.com<br>
                        </p>
                        <div class="social-links mt-3">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">

                </div>


                <div class="col-lg-4 col-md-6">
                    <div class="footer-info">
                        <h3>特别鸣谢</h3>
                        <h4><p class="pb-3" style="font-size: large"><em>该产品背后的每一位成员</em></p></h4>
                        <p>
                            扈必烈 康敏 陈晨<br>周松 姜佩辰 何付炜<br>
                            <br>
                            <strong>中软卓越</strong><br>
                            <strong>创造分享 共同成长</strong><br>
                        </p>
                        <div class="social-links mt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>华晟</span></strong>. All Rights Reserved
        </div>

    </div>
</footer><!-- End Footer -->
<div id="preloader"></div>
<a href="${bp}/index.action" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>


<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/purecounter/purecounter.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

<script>
    function myfun(){

        var phoneNumReg = /^1[3|4|5|7|8]\d{9}$/;
        if (!phoneNumReg.test(document.form.tel.value)){
            alert("请正确输入手机号");
            return false;
        }

        var message1={
            name:$("#name").val(),
            email:$("#email").val(),
            tel:$("#tel").val(),
            subject:$("#subject").val(),
            message:$("#message").val()
        }
        console.log(message1);
        var message=JSON.stringify(message1);
        console.log(message);

        $.ajax({
            type:"post",
            url:"${bp}/getmessage.action",
            contentType:"application/json;charset=utf-8",
            data:message,
            success:function (resp){
                console.log(resp);
                if (resp){
                    alert("您的信息已提交,我们会马上和您联系!");
                }
            }
        });
    }

</script>

</body>

</html>