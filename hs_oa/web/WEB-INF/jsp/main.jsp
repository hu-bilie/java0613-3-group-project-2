<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>

<!DOCTYPE html>
<html lang="en">
<style>
    .button-1 {
        width: 200px;
        height: 100px;
        padding: 20px 15px;
        margin-left: 60px;
        float: left;
    }

    .button-color-1{
        background-color: #053E94;
    }
    .button-color-1:hover {
        background-color: #126677;
    }

    .button-color-2{
        background-color: #7571F9;
    }

    .button-color-2:hover {
        background-color: skyblue;
    }

    .button-color-3{
        background-color: #298DFF;
    }

    .button-color-3:hover {
        background-color: #9ACEBA;
    }

    .button-color-4{
        background-color: #74A041;
    }

    .button-color-4:hover {
        background-color: #91BD46;
    }

    .button-color-5{
        background-color: #2F9F65;
    }

    .button-color-5:hover {
        background-color: #81CA47;
    }

    .button-1-1-1 {
        margin: 5px auto;
    }
</style>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>华晟科技</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->

    <!-- Pignose Calender -->
    <link href="./plugins/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <link href="./css/main.css" rel="stylesheet">
    <!-- Chartist -->
    <link rel="stylesheet" href="./plugins/chartist/css/chartist.min.css">
    <link rel="stylesheet" href="./plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->
<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
         引入头部导航
     ***********************************-->
    <%@include file="head.jsp" %>

    <!--**********************************
        引入外部左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp" %>

    <div class="content-body">

        <div class="container-fluid mt-3">

            <div style="width: 1610px;height: 100px">

                <c:if test="${currentUser.employeeVO.role.rid==2||currentUser.employeeVO.role.rid==3}">
                    <div class="button-1 button-color-1" onclick="window.location.href='${bp}/rad/toprojectlist.action?type=3&eid=0'">
                        <div class="button-1-1">
                            <div class="button-1-1-1">
                                <h2 style="text-align: center;color: white;cursor: pointer" class="mdi mdi-swap-horizontal">:&nbsp;&nbsp;&nbsp;&nbsp;审批</h2>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${currentUser.employeeVO.dept.did==3||currentUser.employeeVO.role.rid==3}">
                <div class="button-1 button-color-2" onclick="window.location.href='${bp}/rad/toaddproject.action'">
                    <div class="button-1-1">
                        <div class="button-1-1-1">
                            <h2 style="text-align: center;color: white;cursor: pointer" class="mdi mdi-file-multiple">
                                :&nbsp;&nbsp;&nbsp;&nbsp;立项</h2>
                        </div>
                    </div>
                </div>
                </c:if>
                <div class="button-1 button-color-3" onclick="window.location.href='${bp}/employee/tostudy.action'">
                    <div class="button-1-1">
                        <div class="button-1-1-1">
                            <h2 style="text-align: center;color: white;cursor: pointer" class="mdi mdi-book-open-variant">
                                :&nbsp;&nbsp;&nbsp;&nbsp;学习</h2>
                        </div>
                    </div>
                </div>

                <div class="button-1 button-color-4" onclick="window.location.href='${bp}/employee/toaddexpense.action'">
                    <div class="button-1-1">
                        <div class="button-1-1-1">
                            <h2 style="text-align: center;color: white;cursor: pointer" class="mdi mdi-database-plus">
                                :&nbsp;&nbsp;&nbsp;&nbsp;报销</h2>
                        </div>
                    </div>
                </div>

                <div class="button-1 button-color-5" onclick="window.location.href='${bp}/employee/toaddleave.action'">
                    <div class="button-1-1">
                        <div class="button-1-1-1">
                            <h2 style="text-align: center;color: white;cursor: pointer" class="mdi mdi-sleep">:&nbsp;&nbsp;&nbsp;&nbsp;请假</h2>
                        </div>
                    </div>
                </div>
            </div>

            <%--<div>
                        <h2 class="card-title text-white">待审批项目</h2>
                        <div >
                            <h2 class="text-white" name="enum1">${enum1}</h2>
                        </div>
                    </div>
    --%>


            <%--
                            <div>
                                <h2 class="card-title text-white">今日在岗人数</h2>
                                <div >
                                    <h2 class="text-white" name="enum2">${enum2}</h2>
                                </div>
                            </div>--%>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">今日在岗员工</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                        <tr>
                                            <th>工号</th>
                                            <th>姓名</th>
                                            <th>性别</th>
                                            <th>电话</th>
                                            <th>部门</th>
                                            <th>职位</th>
                                            <th>状态</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%--          int eid, String ename, String esex, Date birthday, String tel, String address, Role role, Dept dept, Estate estate, Date entrydate--%>
                                        <c:forEach items="${list}" var="employeeVO">
                                            <tr>
                                                <td>
                                                    <a href="${bp}/employee/toemployeeinfo.action?eid=${employeeVO.eid}">${employeeVO.eid}</a>
                                                </td>
                                                <td>
                                                    <a href="${bp}/employee/toemployeeinfo.action?eid=${employeeVO.eid}">${employeeVO.ename}</a>
                                                </td>
                                                <td>${employeeVO.esex}</td>
                                                <td>${employeeVO.tel}</td>
                                                <td>${employeeVO.dept.dname}</td>
                                                <td>${employeeVO.role.rname}</td>
                                                <td>${employeeVO.estate.estate}</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <c:if test="${currentUser.employeeVO.dept.did==3||currentUser.employeeVO.role.rid==2||currentUser.employeeVO.role.rid==3||currentUser.employeeVO.role
                .rid==4}">
                <div class="content-body" style="margin-left: 0">

                    <div class="row page-titles mx-0">
                        <div class="col p-md-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">项目列表</a></li>
                            </ol>
                        </div>
                    </div>
                    <!-- row -->

                    <div class="container-fluid">
                        <div class="row" name="ptable">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">已通过项目</h4>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>项目编号</th>
                                                    <th>项目名称</th>
                                                    <th>项目负责人</th>
                                                    <th>项目立项时间</th>
                                                    <th>项目进度</th>
                                                    <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                        <th>审批</th>
                                                    </c:if>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="project" items="${list1}">
                                                    <tr>
                                                        <td>${project.pid}</td>
                                                        <td>
                                                            <a href="${bp}/rad/toprojectinfo.action?pid=${project.pid}">${project.pname}</a>
                                                        </td>
                                                        <td>
                                                            <a href="${bp}/rad/toprojectlist.action?type=2&eid=${project.employeeVO.eid}"> ${project.employeeVO.ename}</a>
                                                        </td>
                                                        <td><fmt:formatDate value="${project.pcreatetime}"
                                                                            pattern="yyyy-MM-dd hh:mm"></fmt:formatDate></td>
                                                        <td>
                                                            <c:if test="${project.approvestate.asid==2}">
                                                                <span class="float-right">${project.pprogress.ppnum}%</span>
                                                                <div class="progress" style="height: 9px">
                                                                    <div class="progress-bar bg-info wow  progress-" style="width: ${project.pprogress.ppnum}%;" role="progressbar"><span class="sr-only">${project.pprogress.ppnum}%</span>
                                                                    </div>
                                                                </div>
                                                            </c:if>
                                                            <c:if test="${project.approvestate.asid!=2}">
                                                                暂无进度
                                                            </c:if>
                                                        </td>
                                                        <td>
                                                            <c:if test="${currentUser.employeeVO.role.rid==3}">
                                                            <c:if test="${project.approvestate.asid==1}">
                                                            <button class="btn btn-facebook" type="button"
                                                                    onclick="window.location.href='${bp}/rad/toprojectinfo.action?pid=${project.pid}'">
                                                                审批
                                                            </button>
                                                        </td>
                                                        </c:if>
                                                        <c:if test="${project.approvestate.asid==2}">已通过</c:if>
                                                        <c:if test="${project.approvestate.asid==3}">已拒绝</c:if>
                                                        </c:if>
                                                        <c:if test="${currentUser.employeeVO.role.rid!=3}">
                                                            <c:if test="${project.approvestate.asid==1}">待审核</c:if>
                                                            <c:if test="${project.approvestate.asid==2}">已通过</c:if>
                                                            <c:if test="${project.approvestate.asid==3}">已拒绝</c:if>
                                                        </c:if>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>

                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">待审核项目</h4>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>项目编号</th>
                                                    <th>项目名称</th>
                                                    <th>项目负责人</th>
                                                    <th>项目立项时间</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="project" items="${list2}">
                                                    <tr>
                                                        <td>${project.pid}</td>
                                                        <td>
                                                            <a href="${bp}/rad/toprojectinfo.action?pid=${project.pid}">${project.pname}</a>
                                                        </td>
                                                        <td>
                                                            <a href="${bp}/rad/toprojectlist.action?type=2&eid=${project.employeeVO.eid}"> ${project.employeeVO.ename}</a>
                                                        </td>
                                                        <td><fmt:formatDate value="${project.pcreatetime}"
                                                                            pattern="yyyy-MM-dd hh:mm"></fmt:formatDate>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>

                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- #/ container -->
                </div>
            </c:if>
            <!--**********************************
                Content body end
            ***********************************-->
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <!-- Chartjs -->
    <script src="./plugins/chart.js/Chart.bundle.min.js"></script>
    <!-- Circle progress -->
    <script src="./plugins/circle-progress/circle-progress.min.js"></script>
    <!-- Datamap -->
    <script src="./plugins/d3v3/index.js"></script>
    <script src="./plugins/topojson/topojson.min.js"></script>
    <script src="./plugins/datamaps/datamaps.world.min.js"></script>
    <!-- Morrisjs -->
    <script src="./plugins/raphael/raphael.min.js"></script>
    <script src="./plugins/morris/morris.min.js"></script>
    <!-- Pignose Calender -->
    <script src="./plugins/moment/moment.min.js"></script>
    <script src="./plugins/pg-calendar/js/pignose.calendar.min.js"></script>
    <!-- ChartistJS -->
    <script src="./plugins/chartist/js/chartist.min.js"></script>
    <script src="./plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js"></script>


    <script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
    <script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>


    <style>
        .copyrights {
            text-indent: -9999px;
            height: 0;
            line-height: 0;
            font-size: 0;
            overflow: hidden;
        }
    </style>
    <div class="copyrights" id="links20210126">
        Collect from <a href="http://www.cssmoban.com/" title="网站模板">模板之家</a>
        <a href="https://www.chazidian.com/" title="查字典">查字典</a>
    </div>
</body>

</html>