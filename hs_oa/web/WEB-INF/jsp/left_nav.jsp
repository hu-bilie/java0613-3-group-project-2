<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <c:if test="${currentUser.employeeVO.dept.did==5||currentUser.employeeVO.role.rid==3}">
                <li class="nav-label"><b style="font-size: 20px">人事部</b></li>
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="mdi mdi-account"></i><span class="nav-text">员工管理</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="${bp}/employee/toemployeelist.action">员工列表</a></li>
                        <li><a href="${bp}/personnel/toaddemployee.action">新增员工</a></li>
                        <!-- <li><a href="./index-2.html">Home 2</a></li> -->
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i><span class="nav-text">账号管理</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="${bp}/personnel/accountlist.action">账号列表</a></li>
                        <li><a href="${bp}/personnel/toaddaccount.action">添加账号</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="mdi mdi-home"></i><span class="nav-text">部门管理</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="${bp}/personnel/deptlist.action">部门列表</a></li>
                        <li><a href="${bp}/personnel/toadddept.action">新增部门</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="mdi mdi-view-module"></i><span class="nav-text">岗位管理</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="${bp}/personnel/rolelist.action">岗位列表</a></li>
                        <li><a href="${bp}/personnel/toaddrole.action">新增岗位</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="mdi mdi-apps"></i><span class="nav-text">劳动合同管理</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="${bp}/personnel/tolaborcontractlist.action?type=1&eid=0" style="font-size: smaller">劳动合同列表</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="mdi mdi-book-open-page-variant"></i> <span class="nav-text">学习管理</span>
                    </a>
                    <ul aria-expanded="false">
                        <li>
                            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                                <i class="mdi mdi-book-variant"></i><span class="nav-text">学习内容</span>
                            </a>
                            <ul aria-expanded="false">
                                <li><a href="${bp}/employee/tostudy.action" style="font-size: smaller">内容列表</a></li>
                                <li><a href="${bp}/personnel/toaddstudy.action" style="font-size: smaller">新增内容</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="${bp}/personnel/tocollectchart.action">
                                收藏统计
                            </a>
                        </li>
                        </li>
                    </ul>
                </li>
                </li>
            </c:if>
            <c:if test="${currentUser.employeeVO.dept.did==3||currentUser.employeeVO.role.rid==3}">
            <li class="nav-label"><b style="font-size: 20px">研发部</b></li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-border-all"></i><span class="nav-text">项目管理</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-chart-gantt"></i><span class="nav-text">项目列表</span>
                        </a>
                        <ul>
                            <li><a href="${bp}/rad/toprojectlist.action?type=1&eid=0">全部项目</a></li>
                            <li><a href="${bp}/rad/toprojectlist.action?type=3&eid=0">待审核项目</a></li>
                            <li><a href="${bp}/rad/toprojectlist.action?type=4&eid=0">已通过项目</a></li>
                            <li><a href="${bp}/rad/toprojectlist.action?type=5&eid=0">已驳回项目</a></li>
                        </ul>
                    </li>
                    <li><a href="${bp}/rad/toaddproject.action"><span class="nav-text">新增项目</span></a></li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-file-check"></i><span class="nav-text">方案管理</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <a href="${bp}/rad/toschemelist.action"><span class="nav-text">方案列表</span></a>
                        <a href="${bp}/rad/toaddscheme.action">增加新方案</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-book-multiple"></i><span class="nav-text">项目合同管理</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <a href="${bp}/rad/topcontractlist.action?type=1&pid=0&partner=''"><span
                                class="nav-text">项目合同列表</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-chart-bar"></i><span class="nav-text">项目进度管理</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <a href="${bp}/rad/toprogress.action"><span class="nav-text">进度图</span></a>
                    </li>
                </ul>
            </li>
            </c:if>
            <li class="nav-label"><b style="font-size: 20px">日常工作</b></li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-calendar-clock"></i> <span class="nav-text">会议管理</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="${bp}/employee/tomeetinglist.action">会议列表</a></li>
                    <c:if test="${currentUser.employeeVO.role.rid!=1}">
                    <li><a href="${bp}/manager/toaddmeeting.action">新增会议</a></li>
                    </c:if>
                    <li><a href="${bp}/employee/tomeetingnotice.action">会议通知</a></li>
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-clipboard-text"></i> <span class="nav-text">会议记录</span>
                        </a>
                        <ul>
                            <a href="${bp}/employee/torecordlist.action">记录列表</a>
                            <a href="${bp}/employee/toaddrecord.action">新增记录</a>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-bike"></i> <span class="nav-text">请假管理</span>
                </a>
                <ul>
                <c:if test="${currentUser.employeeVO.role.rid==3}">
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-format-align-justify"></i> <span class="nav-text">请假列表</span>
                        </a>
                        <ul>
                            <li><a href="${bp}/employee/toleavelist.action?type=1&eid=0">全部请假列表</a></li>
                            <li><a href="${bp}/employee/toleavelist.action?type=3&eid=0">待审批请假列表</a></li>
                            <li><a href="${bp}/employee/toleavelist.action?type=4&eid=0">已通过请假列表</a></li>
                            <li><a href="${bp}/employee/toleavelist.action?type=5&eid=0">已驳回请假列表</a></li>
                        </ul>
                    </li>
                </c:if>
                    <a href="${bp}/employee/toaddleave.action"><span class="nav-text">申请假期</span></a>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-pipe-leak"></i> <span class="nav-text">报销管理</span>
                </a>
                <ul>
                    <c:if test="${currentUser.employeeVO.role.rid==3}">
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-format-align-justify"></i> <span class="nav-text">报销列表</span>
                        </a>
                        <ul>
                            <li><a href="${bp}/employee/toexpenselist.action?type=1&eid=0">全部报销列表</a></li>
                            <li><a href="${bp}/employee/toexpenselist.action?type=3&eid=0">待审批报销列表</a></li>
                            <li><a href="${bp}/employee/toexpenselist.action?type=4&eid=0">已通过报销列表</a></li>
                            <li><a href="${bp}/employee/toexpenselist.action?type=5&eid=0">已驳回报销列表</a></li>
                        </ul>
                    </li>
                    </c:if>
                    <a href="${bp}/employee/toaddexpense.action"><span class="nav-text">申请报销</span></a>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-book-open-page-variant"></i> <span class="nav-text">我的学习</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-book-variant"></i><span class="nav-text">学习内容</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="${bp}/employee/tostudy.action" style="font-size: smaller">内容列表</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-bookmark-plus"></i><span class="nav-text">我的收藏</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="${bp}/employee/tocollect.action?bmid=-99&aid=${currentUser.aid}"
                                   style="font-size: smaller">收藏内容</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <c:if test="${currentUser.employeeVO.role.rid==3}">
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="mdi mdi-book-variant"></i><span class="nav-text">商业合作</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="${bp}/leader/messagelist.action?type=1">全部咨询</a></li>
                    <li><a href="${bp}/leader/messagelist.action?type=2">已反馈咨询</a></li>
                    <li><a href="${bp}/leader/messagelist.action?type=3">未反馈咨询</a></li>
                </ul>
            </li>
            </c:if>

        </ul>
    </div>
</div>

</html>
