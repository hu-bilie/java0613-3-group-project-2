<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>项目进度管理</title>
    <base href="${bp}/statics/">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body onload="myfun()">


<%@include file="head.jsp" %>

<%@include file="left_nav.jsp" %>


<div id="main" class="content-body">
    <div class="container-fluid" style="margin-bottom:90px;">
        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:history.back(-1)">上一层</a></li>
                </ol>
            </div>

        </div>

    </div>
    <div id="main1" style="margin-left: 50px;height: 600px;width: 600px;margin: auto;"></div>


</div>

<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>
<script type="text/javascript" src="js/echarts.min.js"></script>
<script>
    function myfun(){
        $.post("${bp}/rad/toshow.action",function (data){
            var chartDom = document.getElementById('main1');
            var myChart = echarts.init(chartDom);
            var option;

            option = {
                xAxis: {
                    type: 'category',
                    data: data.names,
                    axisLabel:{
                        rotate:20,
                        fontSize:13
                    }
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        data: data.values,
                        type: 'bar'
                    }
                ]
            };

            option && myChart.setOption(option);
        });
    }
</script>

</body>

</html>





















<%--<body onload="myfun()">--%>

<%--    <!--*******************--%>
<%--        Preloader start--%>
<%--    ********************-->--%>
<%--    <div id="preloader">--%>
<%--        <div class="loader">--%>
<%--            <svg class="circular" viewBox="25 25 50 50">--%>
<%--                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />--%>
<%--            </svg>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <!--*******************--%>
<%--        Preloader end--%>
<%--    ********************-->--%>

<%--    --%>
<%--    <!--**********************************--%>
<%--        Main wrapper start--%>
<%--    ***********************************-->--%>
<%--    <div id="main-wrapper">--%>

<%--        <!--**********************************--%>
<%--            Nav header start--%>
<%--        ***********************************-->--%>
<%--        <div class="nav-header">--%>
<%--            <div class="brand-logo">--%>
<%--                <a href="index.html">--%>
<%--                    <b class="logo-abbr"><img src="images/logo.png" alt=""> </b>--%>
<%--                    <span class="logo-compact"><img src="./images/logo-compact.png" alt=""></span>--%>
<%--                    <span class="brand-title">--%>
<%--                        <img src="images/logo-text.png" alt="">--%>
<%--                    </span>--%>
<%--                </a>--%>
<%--            </div>--%>
<%--        </div>--%>

<%--        <div id="main" style="margin-left: 500px;height: 600px;width: 600px;"></div>--%>
<%--            <!-- #/ container -->--%>
<%--    </div>--%>
<%--        <!--**********************************--%>
<%--            Content body end--%>
<%--        ***********************************-->--%>
<%--        --%>
<%--        --%>
<%--        <!--**********************************--%>
<%--            Footer start--%>
<%--        ***********************************-->--%>

<%--        <!--**********************************--%>
<%--            Footer end--%>
<%--        ***********************************-->--%>
<%--    </div>--%>
<%--    <!--**********************************--%>
<%--        Main wrapper end--%>
<%--    ***********************************-->--%>

<%--    <!--**********************************--%>
<%--        Scripts--%>
<%--    ***********************************-->--%>
<%--    <script src="plugins/common/common.min.js"></script>--%>
<%--    <script src="js/custom.min.js"></script>--%>
<%--    <script src="js/settings.js"></script>--%>
<%--    <script src="js/gleek.js"></script>--%>
<%--    <script src="js/styleSwitcher.js"></script>--%>
<%--    <script src="./js/dashboard/dashboard-2.js"></script>--%>
<%--    <script type="text/javascript" src="js/echarts.min.js"></script>--%>

<%--    --%>
<%--</body>--%>

<%--</html>--%>