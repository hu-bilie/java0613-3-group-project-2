
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>新增请假</title>
    <!-- Favicon icon -->
    <base href="${bp}/statics/">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">
    <%@include file="head.jsp"%>
    <%@include file="left_nav.jsp"%>
    <!--**********************************
        Content body start
    ***********************************-->
    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">新增请假</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">请假管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-validation">
                                <form class="form-valide" action="${bp}/employee/addleave.action" method="post" id="f1" >
                                    <input name="asid" readonly type="hidden" value="1">
                                    <input name="eid" readonly type="hidden" value="${currentUser.employeeVO.eid}">
                                    <div class="form-group row" >
                                        <label for="reason">请假原因<span class="text-danger">&nbsp;&nbsp;</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="reason" name="reason" placeholder="请输入请假原因">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  for="start" >开始时间<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="datetime-local" class="form-control" id="start" name="start1" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  for="end">结束时间<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="datetime-local" class="form-control" id="end" name="end1" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <label >请假类型<span class="text-danger">*</span>
                                            </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="ltid" style="margin-left: 15px">
                                                <c:forEach items="${leavetype}" var="leavetype">
                                                    <option value="${leavetype.ltid}">${leavetype.ltype}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label >请假员工<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="ename" name="ename" readonly value="${currentUser.employeeVO.ename}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="button" class="btn btn-primary" onclick="myfun()">提交</button>
                                            <button type="reset" class="btn btn-primary" >重置</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->



</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/validation/jquery.validate.min.js"></script>
<script src="./plugins/validation/jquery.validate-init.js"></script>

</body>
<script >
    function myfun(){
        if ($("#reason").val().trim()==""){
            alert("请假原因不能为空！")
            return ;
        }
        var start = $("#start").val();
        var end = $("#end").val();
        if (end<=start){
            alert("请假时间不合理,请重新选择");
            return false;
        }
        $("#f1").submit();
    }
</script>

</html>