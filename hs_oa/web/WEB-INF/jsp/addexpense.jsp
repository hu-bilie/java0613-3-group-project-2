
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>新增报销</title>
    <!-- Favicon icon -->
    <base href="${bp}/statics/">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="kindeditor/themes/default/default.css" />

    <script charset="utf-8" src="kindeditor/kindeditor-min.js"></script>
    <script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>
</head>
<%--富文本编辑器--%>
<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea[name="content"]', {
            allowFileManager : true
        });
        K('input[name=getHtml]').click(function(e) {
            alert(editor.html());
        });
        K('input[name=isEmpty]').click(function(e) {
            alert(editor.isEmpty());
        });
        K('input[name=getText]').click(function(e) {
            alert(editor.text());
        });
        K('input[name=selectedHtml]').click(function(e) {
            alert(editor.selectedHtml());
        });
        K('input[name=setHtml]').click(function(e) {
            editor.html('<h3>Hello KindEditor</h3>');
        });
        K('input[name=setText]').click(function(e) {
            editor.text('<h3>Hello KindEditor</h3>');
        });
        K('input[name=insertHtml]').click(function(e) {
            editor.insertHtml('<strong>插入HTML</strong>');
        });
        K('input[name=appendHtml]').click(function(e) {
            editor.appendHtml('<strong>添加HTML</strong>');
        });
        K('input[name=clear]').click(function(e) {
            editor.html('');
        });
    });
</script>
<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->


<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">

    <!--**********************************
           引入头部导航
       ***********************************-->
    <%@include file="head.jsp"%>

    <!--**********************************
        引入外部左侧导航
    ***********************************-->
    <%@include file="left_nav.jsp"%>
    <div class="content-body">
            <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">新增报销</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">报销管理</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-validation">
                                <form class="form-valide" action="${bp}/employee/addexpense.action" method="post" id="f1" enctype="multipart/form-data">
                                    <div class="card-body">
                                        <h4 class="card-title">报销金额</h4>
                                        <div class="form-group">
                                            <input type="number"   class="form-control" id="money" name="money" placeholder="请输入报销金额">
                                        </div>
                                        <h4 class="card-title">员工姓名</h4>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="ename" name="ename" readonly value="${currentUser.employeeVO.ename}">
                                            <input type="hidden" class="form-control" id="eid" name="eid" readonly value="${currentUser.employeeVO.eid}">
                                        </div>
                                        <h4 class="card-title">上传报销资料</h4>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input required type="file" class="custom-file-input" id="file" name="file" >
                                                    <label class="custom-file-label">Choose file</label>
                                                </div>
                                            </div>
                                            <p class="text-muted m-b-15 f-s-12">资料为pdf格式！</p>
                                        </div>
                                        <h4 class="card-title">报销细则</h4>
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <textarea id="content" rows="15" cols="150" class="form-input-content" name="content" ></textarea>
                                                <input type="hidden" name="econtent" id="econtent">
                                            </div>

                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-info" onclick="submit1()" type="button">提交</button>
                                                &nbsp;&nbsp;&nbsp;
                                                <button class="btn btn-outline-danger" type="reset">重置</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
    <!--**********************************
        Content body end
    ***********************************-->


    <!--**********************************
        Footer start
    ***********************************-->

    <!--**********************************
        Footer end
    ***********************************-->
</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

<script src="./plugins/validation/jquery.validate.min.js"></script>
<script src="./plugins/validation/jquery.validate-init.js"></script>





</body>
<script >
    function submit1(){
        if ($("#money").val().trim()==""){
            alert("报销金额不能为空!");
            return false;
        }
        if ($("#file").val().trim()==""){
            alert("请上传报销资料扫描件！");
            return ;
        }
        if(editor.isEmpty()){
            alert("报销细则不能为空!");
            return false;
        }
        alert(editor.html())
        $("#econtent").val(editor.html());
        alert($("#econtent").val())
        $("#f1").submit();
    }
</script>

</html>