package com.hs.dao;

import com.hs.po.Estate;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author ss
 * 员工状态数据访问测试
 */
public class EstateDaoTest {

    Logger logger = Logger.getLogger(EstateDaoTest.class);

    EstateDao estateDao = null;
    @Before
    public void commom(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        estateDao= context.getBean(EstateDao.class);
    }

    @Test
    public void findAll(){
        for (Estate estate:estateDao.findAll()){
            logger.info(estate.getEstate());
        }
    }

    @Test
    public void findById(){
        Estate estate = estateDao.findById(3);
        logger.info(estate.getEstate());
    }
}
