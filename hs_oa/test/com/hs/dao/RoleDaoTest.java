package com.hs.dao;

import com.hs.po.Role;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



/**
 * @author km
 * @author ss
 * 角色数据访问接口测试
 */
public class RoleDaoTest {

    Logger logger = Logger.getLogger(RoleDaoTest.class);

    RoleDao roleDao = null;
    @Before
    public void commom(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        roleDao = context.getBean(RoleDao.class);
    }

    @Test
    public void findAll(){
        for (Role role:roleDao.findAll()){
            logger.info(role.getRname());
        }
    }

    @Test
    public void findById(){
        Role role = roleDao.findById(1);
        logger.info(role);
    }

    @Test
    public void findByRname(){
        Role role = roleDao.findByRname("管理员");
        logger.info(role);
    }

    @Test
    public void addRole(){
        Role role = new Role();
        role.setRname("测试角色");
        int i =  roleDao.addRole(role);
        logger.info(i);
    }

    @Test
    public void updateRole(){
        Role role = roleDao.findById(9);
        logger.info(role);
        role.setRname("测试部门_913");
        roleDao.updateRole(role);
        logger.info(role);
    }


}
