package com.hs.dao;

import com.hs.po.Scheme;
import com.hs.vo.SchemeVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 项目方案数据访问实现类
 */
public class SchemeDaoTest {

    Logger logger = Logger.getLogger(SchemeDaoTest.class);

    SchemeDao schemeDao = null;

    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        schemeDao = context.getBean(SchemeDao.class);
    }

    @Test
    public void findAll(){
        List<Scheme> list = schemeDao.findAll();
        for (Scheme scheme:list){
            logger.info(scheme);
        }
    }

    @Test
    public void add(){
        Scheme scheme = new Scheme();
        scheme.setPid(2);
        scheme.setSctitle("测试方案标题");
        scheme.setScontent("测试方案的内容");
        schemeDao.add(scheme);
        findAll();
    }

    @Test
    public void update(){
        Scheme scheme = schemeDao.findByScid(1);
        scheme.setScontent("修改一下方案的测试内容");
        schemeDao.update(scheme);
        findAll();
    }
    @Test
    public void findByScidVO(){
        SchemeVO schemeVO=schemeDao.findByScidVO(1);
        logger.info(schemeVO.getPname());
    }

}
