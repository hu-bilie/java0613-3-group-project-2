package com.hs.dao;

import com.hs.po.PContract;
import com.hs.po.PcState;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 合同状态数据访问测试类
 */
public class PcStateTest {

    Logger logger = Logger.getLogger(PcStateTest.class);

    PcStateDao pcStateDao = null;

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        pcStateDao = context.getBean(PcStateDao.class);
    }

    @Test
    public void findAll(){
        List<PcState> list = pcStateDao.findAll();
        for (PcState pcState : list){
            logger.info(pcState.getPcstate());
        }
    }
    @Test
    public void findByPcsid(){
        PcState pcState = pcStateDao.findByPcsid(1);
        logger.info(pcState);

    }

}
