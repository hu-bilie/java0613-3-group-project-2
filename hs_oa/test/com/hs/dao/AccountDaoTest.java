package com.hs.dao;

import com.hs.po.Account;
import com.hs.vo.AccountVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 账号数据访问接口测试类
 */
public class AccountDaoTest {

    Logger logger = Logger.getLogger(AccountDaoTest.class);

    AccountDao accountDao = null;

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        accountDao = context.getBean(AccountDao.class);
    }

    @Test
    public void findAll(){
        List<Account> list = accountDao.findAll();
        for (Account account : list){
            logger.info(account);
        }
    }

    @Test
    public void findByAidAndPass(){
        AccountVO account = accountDao.findByAidAndPass(10001,"123123");
        logger.info(account.getEmployeeVO().getTel());
    }

    @Test
    public void findByAid(){
        Account account = accountDao.findByAid(10001);
        logger.info(account);
        account.setUsername("修改后的昵称");
        accountDao.update(account);
        logger.info(account);
    }

    @Test
    public void findVOByAid(){
        AccountVO account=accountDao.findVOByAid(10001);
        logger.info(account);
    }

    @Test
    public void add(){
        Account account = new Account();
        account.setEid(2);
        account.setAstate("已启用");
        account.setPass("123456");
        account.setUsername("新的昵称");
        accountDao.add(account);
        findAll();
    }

    @Test
    public void findByEid(){
        AccountVO account = accountDao.findByEid(1);
        logger.info(account);
    }

}
