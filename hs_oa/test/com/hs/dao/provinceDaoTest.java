package com.hs.dao;

import com.hs.po.Province;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author km
 * 省的数据访问接口测试类
 */
public class provinceDaoTest {
    ProvinceDao provinceDao=null;
    @Before
    public void common(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        System.out.println(context);
        provinceDao=context.getBean(ProvinceDao.class);
    }
    @Test
    public void findAll(){
        for (Province province:provinceDao.findAll()){
            System.out.println(province.getProvinceId()+"\t"+province.getProvince());
        }
    }
    @Test
    public void findObject(){
        Province province=provinceDao.findObject("110000");
        System.out.println(province.getProvinceId()+"\t"+province.getProvince());

    }

}
