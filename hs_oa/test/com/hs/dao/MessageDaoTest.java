package com.hs.dao;

import com.hs.po.Message;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author awei
 * 咨询用户信息数据访问测试类
 */
public class MessageDaoTest {

    MessageDao messageDao = null;
    Logger logger = Logger.getLogger(MessageDaoTest.class);

    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        logger.info(context);
        messageDao = context.getBean(MessageDao.class);
    }
    @Test
    public void findAll(){
        List<Message> list= messageDao.findAll();
        logger.info(list);
    }
    @Test
    public void findById(){
        Message message= messageDao.findById(1);
        logger.info(message.getName());
    }
    @Test
    public void findByTel(){
        Message message= messageDao.findByTel("18223654896");
        logger.info(message.getName());
    }
    @Test
    public void add(){
        Message message=new Message();
        message.setName("周芷若");
        message.setTel("14725836987");
        message.setEmail("124563@qq.com");
        message.setSubject("你好，合作吗");
        message.setMessage("可以出来喝一杯吗");
        message.setSubmittime(new Timestamp(System.currentTimeMillis()));
        message.setFeedback("未反馈");
        int i= messageDao.add(message);
        logger.info(i);
    }

    @Test
    public void update(){
        Message message = messageDao.findById(1);
        message.setFeedback("已反馈");
        messageDao.update(message);
        findAll();
    }

}
