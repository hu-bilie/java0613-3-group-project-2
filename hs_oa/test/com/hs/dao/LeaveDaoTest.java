package com.hs.dao;

import com.hs.po.Leave;
import com.hs.vo.LeaveVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author km
 * 请假数据访问测试类
 */
public class LeaveDaoTest {
    Logger logger = Logger.getLogger(LeaveDaoTest.class);

    LeaveDao leaveDao = null;
    @Before
    public void commom(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        leaveDao = context.getBean(LeaveDao.class);
    }
    @Test
    public void add(){
        findAll();
        //String reason, Timestamp start, Timestamp end, int ltid, int eid, int asid, String lstate
        leaveDao.add(new Leave("生孩子",new Timestamp(1459312934),new Timestamp(213534532),3,1,1,"未消假"));
        findAll();
    }

    @Test
    public void findById() {
        logger.info(leaveDao.findById(1));

    }
    @Test
    public void findVOById() {
        logger.info(leaveDao.findVOById(1));

    }

    @Test
    public void findVOAll(){
        for (LeaveVO leaveVO: leaveDao.findVOAll()){
            logger.info(leaveVO);
        }
    }
    @Test
    public void findAll(){
        for (Leave leave: leaveDao.findAll()){
            logger.info(leave);
        }
    }

    @Test
    public void findByLstate(){
        for (Leave leave: leaveDao.findByLstate("未消假")){
            logger.info(leave);
        }
    }
    @Test
    public void findVOByLstate() {
        for (LeaveVO leave: leaveDao.findVOByLstate("未消假")){
            logger.info(leave);
        }
    }
    @Test
    public void delete() {
        findAll();
        leaveDao.delete(1);
        findAll();
    }
}
