package com.hs.dao;

import com.hs.po.Record;
import com.hs.vo.RecordVO;
import javafx.application.Application;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author 陈晨
 * 会议记录数据访问测试类
 */
public class RecordDaoTest {
    Logger logger=Logger.getLogger(RecordDaoTest.class);

    RecordDao recordDao=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        recordDao=applicationContext.getBean(RecordDao.class);
    }

    @Test
    public void update(){
        Record record=recordDao.findByReid(1);
        record.setMid(1);
        int i=recordDao.update(record);
        logger.info(i);
        logger.info(record);
    }

    @Test
    public void findVOByRtime(){
        List<RecordVO> list=recordDao.findVOByRtime(new Timestamp(2000000000));
        for(RecordVO record:list){
            logger.info(record);
        }
    }
    @Test
    public void findByRtime(){
        List<Record> list=recordDao.findByRtime(new Timestamp(2000000000));
        for(Record record:list){
            logger.info(record);
        }
    }

    @Test
    public void findVOByMid(){
        RecordVO record=recordDao.findVOByMid(2);
        logger.info(record);
    }
    @Test
    public void findByMid(){
        Record record=recordDao.findByMid(2);
        logger.info(record);
    }

    @Test
    public void findByReid(){
        Record record=recordDao.findByReid(1);
        logger.info(record.getContent());
    }

    @Test
    public void findVOByReid(){
        RecordVO record=recordDao.findVOByReid(1);
        logger.info(record);
    }

    @Test
    public void findVOAll(){
        List<RecordVO> list=recordDao.findVOAll();
        for(RecordVO record:list){
            logger.info(record);
        }
    }
    @Test
    public void findAll(){
        List<Record> list=recordDao.findAll();
        for(Record record:list){
            logger.info(record);
        }
    }

    @Test
    public void add(){
        Record record=new Record();
        record.setMid(2);
        record.setContent("针对教师节，我们公司对于老师的福利要有很大的提升");
        record.setRtime(new Timestamp(2000000000));
        int i=recordDao.add(record);
        logger.info(i);
        logger.info(record.getMid()+"\t"+record.getContent());
    }
}
