package com.hs.dao;

import com.hs.po.Area;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author km
 * 区县数据访问接口测试类
 */
public class AreaDaoTest {
    AreaDao areaDao=null;
    @Before
    public void common(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        areaDao=context.getBean(AreaDao.class);
    }
    @Test
    public void findAreaByCid(){
        for (Area area:areaDao.findAreaByCid("110100")){
            System.out.println(area.getAreaId()+"\t"+area.getAreas());
        }
    }
    @Test
    public void findObject(){
       Area area=areaDao.findObject("110101");
       System.out.println(area.getAreaId()+"\t"+area.getAreas());

    }
}
