package com.hs.dao;

import com.hs.po.Collect;
import com.hs.vo.CollectVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author 陈晨
 * 收藏内容数据访问测试类
 */
public class CollectDaoTest {
    Logger logger=Logger.getLogger(CollectDaoTest.class);

    CollectDao collectDao=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        collectDao=applicationContext.getBean(CollectDao.class);
    }

    @Test
    public void delete(){
        int[] arrays={8};
        int i=collectDao.delete(arrays);
        logger.info(i);
        this.findVOAll();
    }

    @Test
    public void update(){
        Collect collect=collectDao.findByCid(5);
        collect.setBmid(3);
        int i=collectDao.update(collect);
        logger.info(i);
        logger.info(collect.getBmid());
    }

    @Test
    public void findVOByStuid(){
        CollectVO collect=collectDao.findVOByStuid(1);

            logger.info(collect);

    }
    @Test
    public void findByStuid(){
        Collect collect=collectDao.findByStuid(1);
            logger.info(collect);
    }

    @Test
    public void findVOByBmid(){
        List<CollectVO> list=collectDao.findVOByBmid(2);
        for(CollectVO collect:list){
            logger.info(collect);
        }
    }
    @Test
    public void findByBmid(){
        List<Collect> list=collectDao.findByBmid(3);
        for(Collect collect:list){
            logger.info(collect);
        }
    }

    @Test
    public void findByCid(){
        Collect collect=collectDao.findByCid(6);
        logger.info(collect.getStuid());
    }

    @Test
    public void findVOByCid(){
        CollectVO collect=collectDao.findVOByCid(5);
        logger.info(collect);
    }

    @Test
    public void findVOAll(){
        List<CollectVO> list=collectDao.findVOAll();
        for(CollectVO collect:list){
            logger.info(collect);
        }
    }
    @Test
    public void findAll(){
        List<Collect> list=collectDao.findAll();
        for(Collect collect:list){
            logger.info(collect);
        }
    }

    @Test
    public void add(){
        Collect collect=new Collect();
        collect.setStuid(1);
        collect.setBmid(2);
        int i=collectDao.add(collect);
        logger.info(i);
        logger.info(collect);
    }
}
