package com.hs.dao;

import com.hs.po.Dept;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author ss
 * 部门数据访问测试
 */
public class DeptDaoTest {

    Logger logger = Logger.getLogger(DeptDaoTest.class);

    DeptDao deptDao =null;
    @Before
    public void commmom(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        deptDao = context.getBean(DeptDao.class);
    }

    @Test
    public void findAll(){
        List<Dept> list = deptDao.findAll();
        for (Dept dept:list){
            logger.info(dept.getDname());
        }
    }

    @Test
    public void findByDid(){
        Dept dept = deptDao.findByDid(1);
        logger.info(dept.getDname());
    }

    @Test
    public void findByDname(){
        Dept dept = deptDao.findByDname("测试部1");
        logger.info(dept);
    }

    @Test
    public void addDept(){
        Dept dept = new Dept();
        dept.setDname("测试部门4");
        deptDao.addDept(dept);
    }

    @Test
    public void updateDept(){
        Dept dept = deptDao.findByDid(9);
        logger.info(dept);
        dept.setDname("测试部门_913");
        deptDao.updateDept(dept);
        logger.info(dept);
    }

}

