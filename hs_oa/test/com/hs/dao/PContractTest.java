package com.hs.dao;

import com.hs.po.PContract;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 项目合同数据访问测试类
 */
public class PContractTest {

    Logger logger = Logger.getLogger(PContractTest.class);

    PContractDao pContractDao = null;

    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        pContractDao = context.getBean(PContractDao.class);
    }

    @Test
    public void findAll() {
        List<PContract> list = pContractDao.findAll();
        for (PContract pContract : list) {
            logger.info(pContract);
        }
    }

    @Test
    public void findByPcid() {
        PContract pContract = pContractDao.findByPcid("2");

        logger.info(pContract);

    }

}
