package com.hs.dao;

import com.hs.po.Area;
import com.hs.po.City;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author km
 * 城市数据访问接口测试类
 */
public class CityDaoTest {
    CityDao cityDao=null;
    @Before
    public void common(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        cityDao=context.getBean(CityDao.class);
    }
    @Test
    public void findCityByPid(){
        for (City city:cityDao.findCityByPid("120000")){
            System.out.println(city.getCityId()+"\t"+city.getCity());
        }
    }
    @Test
    public void findObject(){
       City city=cityDao.findObject("110100");
        System.out.println(city.getCityId()+"\t"+city.getCity());

    }
}
