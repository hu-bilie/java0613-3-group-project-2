package com.hs.dao;

import com.hs.po.Employee;
import com.hs.vo.EmployeeVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author ss
 * 员工数据访问接口测试
 */
public class EmployeeDaoTest {

    Logger logger = Logger.getLogger(EmployeeDaoTest.class);

    EmployeeDao employeeDao = null;

    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        employeeDao = context.getBean(EmployeeDao.class);
    }

    @Test
    public void findAll() {
        for (EmployeeVO employeeVO : employeeDao.findAll()) {
            logger.info(employeeVO);
        }
    }

    @Test
    public void findByEid() {
        EmployeeVO employeeVO = employeeDao.findVOByEid(1);
        logger.info(employeeVO);
    }

    @Test
    public void findByName() {

        for (EmployeeVO employeeVO : employeeDao.findByName("张")) {
            logger.info(employeeVO);
        }
    }

    @Test
    public void findByRid() {
        for (EmployeeVO employeeVO : employeeDao.findByRid(1)) {
            logger.info(employeeVO);
        }
    }

    @Test
    public void updataEmployee(){
        Employee employee = employeeDao.findByEid1(3);
//      String ename, String esex, Date birthday, String tel, String address, int rid, int did, int esid, Date entrydate
        employee.setEname("测试员工1");
        employee.setEsex("女");
        employee.setBirthday(new Date(2000));
        employee.setTel("17802971388");
        employee.setAddress("山东省");
        employee.setRid(10);
        employee.setDid(5);
        employee.setEsid(1);
        employee.setEntrydate(new Date(4000));
        employeeDao.updataEmployee(employee);
    }

    @Test
    public void addEmployee(){
//        String ename, String esex, Date birthday, String tel, String address, int rid, int did, int esid, Date entrydate
        Employee employee = new Employee("王五","男",new Date(8000),"950800","甘肃",1,2,1,new Date(90000));
        employeeDao.addEmployee(employee);

    }




}























