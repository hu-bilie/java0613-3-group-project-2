package com.hs.dao;

import com.hs.po.LaborContract;
import com.hs.po.LcState;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/*
 * @author awei
 * 劳动合同状态数据访问测试类
 */
public class LcStateDaoTest {
    LcStateDao lcStateDao = null;
    Logger logger = Logger.getLogger(LcStateDaoTest.class);

    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        logger.info(context);
        lcStateDao = context.getBean(LcStateDao.class);
    }
    @Test
    public void findAll(){
        List<LcState> list=lcStateDao.findAll();
        for (LcState lcState:list){
            logger.info(lcState.getLcsid());
        }
    }
    @Test
    public void findByLcsid(){
        LcState lcState =lcStateDao.findByLcsid(1);
        logger.info(lcState);
    }
}
