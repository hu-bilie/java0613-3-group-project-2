package com.hs.dao;

import com.hs.po.Project;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author bilie
 * 项目立项测试类
 */
public class ProjectTest {

    ProjectDao projectDao = null;

    Logger logger = Logger.getLogger(ProjectTest.class);

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        projectDao = context.getBean(ProjectDao.class);
        logger.info(projectDao);
    }

    @Test
    public void findAll(){
       List<Project> list = projectDao.findAll();
       for (Project project:list){
           logger.info(project);
       }
    }

    @Test
    public void add(){
        Project project = new Project();
        project.setPname("第一个测试项目");
        project.setPcontent("测试项目的内容");
        project.setEid(1);
        project.setPcreatetime(new Timestamp(System.currentTimeMillis()));
        project.setAsid(1);
        project.setPpid(1);
        logger.info(projectDao.add(project));
        findAll();
    }

    @Test
    public void count(){
        logger.info(projectDao.count());
    }
}
