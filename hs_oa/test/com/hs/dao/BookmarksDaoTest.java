package com.hs.dao;

import com.hs.po.Bookmarks;
import com.hs.vo.BookmarksVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author 陈晨
 * 收藏夹数据访问测试类
 */
public class BookmarksDaoTest {
    Logger logger=Logger.getLogger(BookmarksDaoTest.class);

    BookmarksDao bookmarksDao=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        bookmarksDao=applicationContext.getBean(BookmarksDao.class);
    }

    @Test
    public void delete(){
        int i=bookmarksDao.delete(2);
        logger.info(i);
        this.findAll();
    }

    @Test
    public void update(){
        Bookmarks bookmarks=bookmarksDao.findByBmid(1);
        bookmarks.setBmname("前端");
        int i=bookmarksDao.update(bookmarks);
        logger.info(i);
        this.findAll();
    }
    @Test
    public void findVOByAid(){
        List<BookmarksVO> list=bookmarksDao.findVOByAid(1);
        for(BookmarksVO bookmarks:list){
            logger.info(bookmarks);
        }
    }

    @Test
    public void findByAid(){
        List<Bookmarks> list=bookmarksDao.findByAid(1);
        for(Bookmarks bookmarks:list){
            logger.info(bookmarks);
        }
    }

    @Test
    public void findVOByBmid(){
        BookmarksVO bookmarks=bookmarksDao.findVOByBmid(1);
        logger.info(bookmarks);
    }
    @Test
    public void findByBmid(){
        Bookmarks bookmarks=bookmarksDao.findByBmid(1);
        logger.info(bookmarks);
    }

    @Test
    public void findVOAll(){
        List<BookmarksVO> list=bookmarksDao.findVOAll();
        for(BookmarksVO bookmarks:list){
            logger.info(bookmarks);
        }
    }
    @Test
    public void findAll(){
        List<Bookmarks> list=bookmarksDao.findAll();
        for(Bookmarks bookmarks:list){
            logger.info(bookmarks);
        }
    }

    @Test
    public void add(){
        Bookmarks bookmarks=new Bookmarks();
        bookmarks.setBmname("java");
        bookmarks.setAid(10001);
        int i=bookmarksDao.add(bookmarks);
        logger.info(i);
        logger.info(bookmarks);
    }

}
