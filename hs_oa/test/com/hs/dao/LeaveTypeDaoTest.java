package com.hs.dao;

import com.hs.po.LeaveType;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author jpc
 * 请假状态数据访问测试
 */
public class LeaveTypeDaoTest {
    Logger logger = Logger.getLogger(LeaveTypeDaoTest.class);
    LeaveTypeDao leaveTypeDao = null;
    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        leaveTypeDao = context.getBean(LeaveTypeDao.class);
    }
    @Test
    public void findAll(){
        List<LeaveType> list = leaveTypeDao.findAll();
        logger.info(list);
        for(LeaveType leaveType:list){
            logger.info(leaveType.getLtid()+"\t"+leaveType.getLtype());
        }
    }
    @Test
    public void findById(){
        LeaveType leaveType = leaveTypeDao.findById(7);
        logger.info(leaveType.getLtid()+"\t"+leaveType.getLtype());
    }

}
