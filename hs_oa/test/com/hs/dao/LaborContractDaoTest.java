package com.hs.dao;

import com.hs.po.LaborContract;
import com.hs.vo.LaborContractVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author awei
 * 劳动合同数据访问测试类
 */
public class LaborContractDaoTest {

    LaborContractDao laborContractDao = null;
    Logger logger = Logger.getLogger(LaborContractDaoTest.class);
    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        logger.info(context);
        laborContractDao = context.getBean(LaborContractDao.class);
    }
    @Test
    public void findAll(){
        List<LaborContract> list=laborContractDao.findAll();
        logger.info(list);
        for (LaborContract laborContract:list){
            logger.info(laborContract.getLcaddr());
        }
    }
    @Test
    public void findAllVO(){
        List<LaborContract> list=laborContractDao.findAll();
        logger.info(list);
        for (LaborContract laborContract:list){
            logger.info(laborContract.getLcaddr());
        }
    }
    @Test
    public void findByEid(){
       for(LaborContract laborContract:laborContractDao.findByEid(1)){
           logger.info(laborContract.getLcaddr());
       };

    }
    @Test
    public void add() {
        LaborContract laborContract=new LaborContract();
        laborContract.setLcid("1245125");
        laborContract.setLcaddr("hfjhfsf");
        laborContract.setLcsid(1);
        laborContract.setEid(1);
        laborContract.setLcstart(new Timestamp(System.currentTimeMillis()));
        laborContract.setLcstop(new Timestamp(System.currentTimeMillis()));
        logger.info(laborContractDao.add(laborContract));
    }
    @Test
    public void update(){
        LaborContract laborContract=laborContractDao.findByLcid("1");
        laborContract.setLcstart(new Timestamp(System.currentTimeMillis()));
        logger.info(laborContractDao.update(laborContract));
    }

    @Test
    public void find(){
        LaborContractVO laborContractVO = laborContractDao.findVOByLcid("123456");
        logger.info(laborContractVO);
    }
    @Test
    public void findVOByLcsid(){
        List<LaborContractVO> list= laborContractDao.findVOByLcsid(1);
        logger.info(list);
    }
}
