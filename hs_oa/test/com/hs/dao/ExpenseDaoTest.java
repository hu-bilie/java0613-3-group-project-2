package com.hs.dao;

import com.hs.po.Expense;
import com.hs.vo.ExpenseVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author jpc
 * 报销数据访问接口测试
 */
public class ExpenseDaoTest {
    Logger logger = Logger.getLogger(ExpenseDaoTest.class);
    ExpenseDao expenseDao = null;

    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        expenseDao = context.getBean(ExpenseDao.class);
    }

    @Test
    public void findVOAll() {
        List<ExpenseVO> list = expenseDao.findVOAll();
        logger.info(list);
        for (ExpenseVO expense : list) {
            logger.info(expense.getExid());
        }
    }

    @Test
    public void add() {
        Expense expense = new Expense();
        expense.setExid(4);
        expense.setMoney(1779.1);
        expense.setEid(2);
        expense.setEfaddr("陕西省");
        expense.setAsid(3);
        int i = expenseDao.add(expense);
        logger.info(i);
        this.findVOAll();
    }

    @Test
    public void findById() {
        ExpenseVO expense = expenseDao.findVOByExId(1);
        logger.info(expense.getMoney() + "\t" + expense.getEfaddr());
    }

    @Test
    public void delete() {
    findVOAll();
    int i = expenseDao.delete(4);
    logger.info(i);
    findVOAll();
    }
}
