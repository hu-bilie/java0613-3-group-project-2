package com.hs.dao;


import com.hs.po.PProgress;
import com.hs.vo.Progress;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 项目进度数据访问测试类
 */
public class PProgressDaoTest {

    Logger logger = Logger.getLogger(PProgressDaoTest.class);

    PProgressDao pProgressDao = null;

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        pProgressDao = context.getBean(PProgressDao.class);
    }

    @Test
    public void findAll(){
        List<PProgress> list = pProgressDao.findall();
        for (PProgress pProgress : list){
            logger.info(pProgress.getPpid());
        }
    }

    @Test
    public void add(){
        PProgress pProgress = new PProgress();
        pProgress.setPpnum(20);
        pProgressDao.add(pProgress);
        findAll();
    }

    @Test
    public void update(){
        PProgress pProgress = pProgressDao.findByPpid(2);
        logger.info(pProgress.getPpnum());
        pProgress.setPpnum(50);
        pProgressDao.update(pProgress);
        logger.info(pProgress.getPpnum());
    }

    @Test
    public void findnum(){
        List<Progress> list = pProgressDao.findNum();
        for (Progress progress:list){
            logger.info(progress.getPname());
        }

    }

}
