package com.hs.dao;

import com.hs.po.Study;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author awei
 * 学习内容数据访问测试类
 */
public class StudyDaoTest {

    StudyDao studyDao = null;
    Logger logger = Logger.getLogger(StudyDaoTest.class);

    @Before
    public void common() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        logger.info(context);
        studyDao = context.getBean(StudyDao.class);
    }

    @Test
    public void findAll() {
        List<Study> list = studyDao.findAll();
        logger.info(list);
        for (Study study : list) {
            logger.info(study.getStutitle());
        }
    }

    @Test
    public void findByStuid() {
        Study study = studyDao.findByStuid(1);
        logger.info(study.getStutitle());
    }

    @Test
    public void addStudy() {
        Study study = new Study();
        study.setStutitle("c#");
        studyDao.addStudy(study);
    }

    @Test
    public void update() {
        Study study = studyDao.findByStuid(5);
        study.setStutitle("mybatis");
        study.setStuaddr("测试地址5");
        int i = studyDao.update(study);
        logger.info(i);
    }

    @Test
    public void delete() {
        int i = studyDao.delete(5);
        logger.info(i);
    }

}
