package com.hs.dao;

import com.hs.po.Meeting;
import com.hs.vo.MeetingVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author cc
 * 会议数据访问测试类
 */
public class MeetingDaoTest {

    Logger logger=Logger.getLogger(MeetingDaoTest.class);
    MeetingDao meetingDao=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/spring-dao.xml");
        meetingDao=applicationContext.getBean(MeetingDao.class);
    }

    @Test
    public void update(){
        Meeting meeting=meetingDao.findByMid(2);
        meeting.setDid(4);
        meeting.setMtitle("教师节活动");
        meeting.setMeetingstart(new Timestamp(2000000000));
        meeting.setMeetingstop(new Timestamp(2000100000));
        int i=meetingDao.update(meeting);
        logger.info(i);
        this.findAll();
    }

    @Test
    public void findVOBymeetingstart(){
        List<MeetingVO> list=meetingDao.findVOByMeetingstart(new Timestamp(12000000));
        for(MeetingVO meeting:list){
            logger.info(meeting);
        }
    }
    @Test
    public void findBymeetingstart(){
        List<Meeting> list=meetingDao.findByMeetingstart(new Timestamp(12000000));
        for(Meeting meeting:list){
            logger.info(meeting);
        }
    }

    @Test
    public void findVOByMid(){
        MeetingVO meeting=meetingDao.findVOByMid(1);
        logger.info(meeting);
    }
    @Test
    public void findByMid(){
        Meeting meeting=meetingDao.findByMid(1);
        logger.info(meeting);
    }

    @Test
    public void findVOByMtitle(){
        List<MeetingVO> list=meetingDao.findVOByMtitle("秋");
        for(MeetingVO meeting:list){
            logger.info(meeting);
        }
    }
    @Test
    public void findByMtitle(){
        List<Meeting> list=meetingDao.findByMtitle("秋");
        for(Meeting meeting:list){
            logger.info(meeting);
        }
    }

    @Test
    public void add(){
        Meeting meeting=new Meeting();
        meeting.setMtitle("中秋节福利");
        meeting.setDid(2);
        meeting.setMeetingstart(new Timestamp(12000000));
        meeting.setMeetingstop(new Timestamp(1200000000));
        int i=meetingDao.add(meeting);
        logger.info(i);
        this.findAll();
    }

    @Test
    public void findVOAll(){
        List<MeetingVO> list=meetingDao.findVOAll();
        for(MeetingVO meeting:list){
            logger.info(meeting);
        }
    }
    @Test
    public void findAll(){
        List<Meeting> list=meetingDao.findAll();
        for(Meeting meeting:list){
            logger.info(meeting);
        }
    }

    @Test
    public void findAllAsc(){
        List<Meeting> list=meetingDao.findAllAsc();
        for(Meeting meeting:list){
            logger.info(meeting);
        }
    }

}
