package com.hs.service;


import com.hs.po.Area;
import com.hs.service.AreaService;
import com.hs.vo.AccountVO;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author km
 * 区县业务逻辑测试类
 */
public class AreaServiceTest {
    AreaService areaService=null;
    @Before
    public void common(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/*.xml");
        areaService=context.getBean(AreaService.class);
    }
    @Test
    public void findAreaByCid(){
        for (Area area:areaService.findAreaByCid("110100")){
            System.out.println(area.getAreaId()+"\t"+area.getAreas());
        }
    }
    @Test
    public void findObject(){
       Area area=areaService.findObject("110101");
       System.out.println(area.getAreaId()+"\t"+area.getAreas());

    }


}
