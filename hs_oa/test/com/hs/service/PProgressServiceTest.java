package com.hs.service;

import com.hs.po.PProgress;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 方案进度业务逻辑测试类
 */
public class PProgressServiceTest {

    PProgressService pProgressService = null;

    Logger logger = Logger.getLogger(PProgressServiceTest.class);

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        pProgressService = context.getBean(PProgressService.class);
    }

    @Test
    public void findAll(){
        List<PProgress> list = pProgressService.findall();
        for (PProgress pProgress:list){
            logger.info(pProgress.getPpnum()+"\t"+ pProgress.getPpid());
        }
    }

    @Test
    public void findByPpid(){
        PProgress pProgress = pProgressService.findByPpid(1);
        logger.info(pProgress.getPpnum());
    }

    @Test
    public void add(){
        PProgress pProgress = new PProgress();
        pProgress.setPpnum(30);
        pProgressService.add(pProgress);
        findAll();
    }

    @Test
    public void update(){
        PProgress pProgress = pProgressService.findByPpid(1);
        pProgress.setPpnum(20);
        pProgressService.update(pProgress);
        findAll();
    }

}
