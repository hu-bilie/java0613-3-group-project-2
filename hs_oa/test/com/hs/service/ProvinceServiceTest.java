package com.hs.service;

import com.hs.po.Province;
import com.hs.service.ProvinceService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author km
 * 省级业务逻辑接口测试类
 */
public class ProvinceServiceTest {
    ProvinceService provinceService=null;
    @Before
    public void common(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/*.xml");
        System.out.println(context);
        provinceService=context.getBean( ProvinceService.class);
    }
    @Test
    public void findAll(){
        for (Province province:provinceService.findAll()){
            System.out.println(province.getProvinceId()+"\t"+province.getProvince());
        }
    }
    @Test
    public void findObject(){
        Province province=provinceService.findObject("110000");
        System.out.println(province.getProvinceId()+"\t"+province.getProvince());

    }

}
