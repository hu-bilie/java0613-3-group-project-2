package com.hs.service;

import com.hs.po.Employee;
import com.hs.vo.EmployeeVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Date;
import java.util.List;

/**
 * @author ss
 * 员工业务逻辑实现类测试
 */
public class EmployeeServiceImplTest {

    Logger logger = Logger.getLogger(EmployeeServiceImplTest.class);
    EmployeeService employeeService = null;
    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        employeeService = context.getBean(EmployeeService.class);
    }

    @Test
    public void findAll(){
        for (EmployeeVO employeeVO:employeeService.findAll()){
            logger.info(employeeVO.getEname());
        }
    }

    @Test
    public void findNotlizhi(){
        for (EmployeeVO employeeVO:employeeService.findNotlizhi()){
            logger.info(employeeVO);
        }
    }


    @Test
    public void findByEid(){
        logger.info(employeeService.findVOByEid(1));
    }

    @Test
    public void findByName(){
        for (EmployeeVO employeeVO:employeeService.findByName("张")){
            logger.info(employeeVO);
        }
    }

    @Test
    public void findByRid(){
        for (EmployeeVO employeeVO:employeeService.findByRid(1)){
            logger.info(employeeVO);
        }
    }

    @Test
    public void findByPid(){
        for (EmployeeVO employeeVO:employeeService.findByPid(1)){
            logger.info(employeeVO);
        }
    }

    @Test
    public void updataEmployee(){
        Employee employee = employeeService.findByEid1(3);
        logger.info(employee);
        employee.setEname("测试员工913");
        boolean f = employeeService.updataEmployee(employee);
        logger.info(employee);
        logger.info(f);
    }

    @Test
    public void addEmployee(){
        Employee employee = new Employee();
        employee.setEname("测试员工913_2");
        employee.setEsex("女");
        employee.setBirthday(new Date(2000));
        employee.setTel("17802971388");
        employee.setAddress("山东省");
        employee.setRid(10);
        employee.setDid(5);
        employee.setEsid(1);
        employee.setEntrydate(new Date(4000));
        boolean f = employeeService.addEmployee(employee);
        logger.info(f);
    }

    @Test
    public void count() {
        logger.info(employeeService.count());
    }
}
