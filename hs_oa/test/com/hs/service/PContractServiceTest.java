package com.hs.service;

import com.hs.dao.PContractDao;
import com.hs.dao.ProjectDao;
import com.hs.po.PContract;
import com.hs.vo.PContractVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;

/**
 * @author km
 * 项目合同数据访问测试类
 */
public class PContractServiceTest {

    Logger logger = Logger.getLogger(PContractServiceTest.class);

    PContractService pContractDao = null;
    ProjectService projectDao=null;
    @Before
    public void commom(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        pContractDao = context.getBean(PContractService.class);
        projectDao=context.getBean(ProjectService.class);
    }
    @Test
    public void findAll() {
        for (PContract pContract:pContractDao.findAll()){
            logger.info(pContract);
        }
    }
    @Test
    public void findVOAll(){
        for (PContractVO pContractVO:pContractDao.findVOAll()){
            logger.info(pContractVO);
        }
    }
    @Test
    public void findByPid() {
        for(PContract pContract:pContractDao.findByPid(2)){
            logger.info(pContract);
        }
    }
    @Test
    public void findVOByPid() {
        for(PContractVO pContractVO:pContractDao.findVOByPid(5)){
            logger.info(pContractVO);
        }
    }

    @Test
    public void findByPcid() {
        logger.info(pContractDao.findByPcid("21423334"));
    }
    @Test
    public void findVOByPcid() {
        logger.info(pContractDao.findVOByPcid("11201278643Ac12134"));
    }
    @Test
    public void findByPartner() {
        for(PContract pContract:pContractDao.findByPartner("vivo")){
            logger.info(pContract);
        };
    }
    @Test
    public void findVOByPartner() {
        for(PContractVO pContractVO:pContractDao.findVOByPartner("vivo")){
            logger.info(pContractVO);
        }
    }

    @Test
    public void add() {
        findAll();
        //String pcid, String pcaddr, String partner, int pid, Timestamp pcstart, Timestamp pcstop, int pcsid
        pContractDao.add(new PContract("11201278643Ac12134","http://www.ass.com","oppo",2,new Timestamp(System.currentTimeMillis()),new Timestamp(1467749146),1));
        findAll();
    }
    @Test
    public void update() {
        findAll();
        PContract pContract=pContractDao.findByPcid("1214512");
        pContract.setPartner("vivo");
        pContractDao.update(pContract);
        findAll();
    }

}
