package com.hs.service;

import com.hs.dao.RoleDao;
import com.hs.po.Role;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author km
 * 角色业务逻辑测试类
 */
public class RoleServiceTest {
    RoleService roleService=null;
    @Before
    public void common(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/*.xml");
        System.out.println(context);
        roleService=context.getBean(RoleService.class);
    }
    @Test
    public void findAll(){
        System.out.println(roleService);
        for(Role role:roleService.findAll()){
            System.out.println(role.getRid()+"\t"+role.getRname());
        }
    }
    @Test
    public void findById(){
        System.out.println(roleService);
        Role role=roleService.findById(1);
        System.out.println(role.getRid()+"\t"+role.getRname());

    }
    @Test
    public void add(){
        System.out.println(roleService);
        this.findAll();
        roleService.addRole(new Role("测试角色1"));
        this.findAll();
    }
    @Test
    public void update(){
        System.out.println(roleService);
        this.findAll();
        Role role=roleService.findById(15);
        role.setRname("不想测试的角色");
        roleService.updateRole(role);
        this.findAll();
    }
}