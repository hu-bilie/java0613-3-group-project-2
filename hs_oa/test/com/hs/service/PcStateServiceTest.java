package com.hs.service;

import com.hs.po.PContract;
import com.hs.po.PcState;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 合同状态的业务逻辑测试类
 */
public class PcStateServiceTest {

    PcStateService pcStateService = null;

    Logger logger = Logger.getLogger(PcStateServiceTest.class);

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        pcStateService = context.getBean(PcStateService.class);
    }

    @Test
    public void findAll(){
        List<PcState> list = pcStateService.findAll();
        for (PcState pcState:list){
            logger.info(pcState.getPcstate());
        }
    }
    @Test
    public void findByPcsid(){
        PcState pcState = pcStateService.findByPcsid(1);

    }

}
