package com.hs.service;

import com.hs.po.Dept;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author ss
 * 部门业务逻辑实现类测试
 */
public class DeptServiceImplTest {

    Logger logger = Logger.getLogger(DeptServiceImplTest.class);

    DeptService deptService = null;
    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        deptService = context.getBean(DeptService.class);
    }

    @Test
    public void findAll(){
        for (Dept dept:deptService.findAll()){
            logger.info(dept.getDname());
        }
    }

    @Test
    public void findByDid(){
        logger.info(deptService.findByDid(1));
    }

    @Test
    public void addDept(){
        Dept dept = new Dept();
        dept.setDname("测试912部门");
        logger.info(deptService.addDept(dept));
    }

    @Test
    public void update(){
        Dept dept = deptService.findByDid(10);
        dept.setDname("测试部门_914-2");
        logger.info(deptService.updeteDept(dept));
    }
}
