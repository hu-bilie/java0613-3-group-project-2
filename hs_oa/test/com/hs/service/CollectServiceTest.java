package com.hs.service;

import com.hs.dao.CollectDao;
import com.hs.dao.CollectDaoTest;
import com.hs.po.Collect;
import com.hs.vo.CollectVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author 陈晨
 * 收藏内容业务逻辑测试类
 */
public class CollectServiceTest {
    Logger logger=Logger.getLogger(CollectDaoTest.class);

    CollectService collectService=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/*.xml");
        collectService=applicationContext.getBean(CollectService.class);
    }

    @Test
    public void delete(){
        int[] arrays={8};
        boolean i=collectService.delete(arrays);
        logger.info(i);
        this.findVOAll();
    }

    @Test
    public void update(){
        Collect collect=collectService.findByCid(5);
        collect.setBmid(3);
        boolean i=collectService.update(collect);
        logger.info(i);
        logger.info(collect.getBmid());
    }

    @Test
    public void findByStuid(){
        CollectVO collect=collectService.findVOByStuid(1);

            logger.info(collect);

    }

    @Test
    public void findByBmid(){
        List<CollectVO> list=collectService.findVOByBmid(3);
        for(CollectVO collect:list){
            logger.info(collect);
        }
    }

    @Test
    public void findByCid2(){
        Collect collect=collectService.findByCid(6);
        logger.info(collect.getStuid());
    }

    @Test
    public void findByCid(){
        CollectVO collect=collectService.findVOByCid(5);
        logger.info(collect);
    }

    @Test
    public void findVOAll(){
        List<CollectVO> list=collectService.findVOAll();
        for(CollectVO collect:list){
            logger.info(collect.getStudy().getStutitle());
        }
    }

    @Test
    public void add(){
        Collect collect=new Collect();
        collect.setStuid(1);
        collect.setBmid(3);
        boolean i=collectService.add(collect);
        logger.info(i);
        logger.info(collect.getStuid()+"\t"+collect.getBmid());
    }
}
