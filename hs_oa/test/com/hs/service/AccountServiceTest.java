package com.hs.service;

import com.hs.vo.AccountVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author ss
 * 账号业务逻辑测试类
 */
public class AccountServiceTest {

    Logger logger = Logger.getLogger(AccountServiceTest.class);

    AccountService accountService = null;
    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/*.xml");
        accountService = context.getBean(AccountService.class);
    }

    @Test
    public void findVOAll(){
        List<AccountVO> list = accountService.findVOAll();
        logger.info(list);
    }

}
