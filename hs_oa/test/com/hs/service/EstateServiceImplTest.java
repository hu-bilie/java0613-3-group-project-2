package com.hs.service;

import com.hs.po.Estate;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author ss
 * 员工状态业务逻辑实现类测试
 */
public class EstateServiceImplTest {

    Logger logger = Logger.getLogger(EstateServiceImplTest.class);

    EstateService estateService = null;
    @Before
    public void commom(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        estateService=context.getBean(EstateService.class);
    }

    @Test
    public void findAll(){
        for (Estate estate:estateService.findAll()){
            logger.info(estate.getEstate());
        }
    }

    @Test
    public void findById(){
        logger.info(estateService.findById(1).getEstate());
    }
}



