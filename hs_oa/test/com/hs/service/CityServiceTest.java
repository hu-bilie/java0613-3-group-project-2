package com.hs.service;

import com.hs.service.CityService;
import com.hs.dao.CityDao;
import com.hs.po.City;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author km
 * 市的业务逻辑测试类
 */
public class CityServiceTest {
    CityService cityService=null;
    @Before
    public void common(){
        ApplicationContext context=new ClassPathXmlApplicationContext("spring/*.xml");
        cityService=context.getBean(CityService.class);
    }
    @Test
    public void findCityByPid(){
        for (City city:cityService.findCityByPid("120000")){
            System.out.println(city.getCityId()+"\t"+city.getCity());
        }
    }
    @Test
    public void findObject(){
       City city=cityService.findObject("110100");
        System.out.println(city.getCityId()+"\t"+city.getCity());

    }
}
