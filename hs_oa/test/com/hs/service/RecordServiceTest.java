package com.hs.service;

import com.hs.dao.RecordDao;
import com.hs.dao.RecordDaoTest;
import com.hs.po.Record;
import com.hs.vo.RecordVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author 陈晨
 * 会议记录业务逻辑测试类
 */
public class RecordServiceTest {
    Logger logger=Logger.getLogger(RecordDaoTest.class);

    RecordService recordService=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/*.xml");
        recordService=applicationContext.getBean(RecordService.class);
    }

    @Test
    public void update(){
        Record record=recordService.findByReid(1);
        record.setMid(1);
        boolean i=recordService.update(record);
        logger.info(i);
        logger.info(record.getMid()+"\t"+record.getContent());
    }

    @Test
    public void findVOByRtime(){
        List<RecordVO> list=recordService.findVOByRtime(new Timestamp(2000000000));
        for(RecordVO record:list){
            logger.info(record);
        }
    }

    @Test
    public void findVOByMid(){
        RecordVO record=recordService.findVOByMid(2);
        logger.info(record);

    }

    @Test
    public void findByReid(){
        Record record=recordService.findByReid(1);
        logger.info(record.getContent());
    }

    @Test
    public void findVOByReid(){
        RecordVO record=recordService.findVOByReid(1);
        logger.info(record);
    }

    @Test
    public void findVOAll(){
        List<RecordVO> list=recordService.findVOAll();
        for(RecordVO record:list){
            logger.info(record);
        }
    }

    @Test
    public void add(){
        Record record=new Record();
        record.setMid(2);
        record.setContent("针对教师节，我们公司对于老师的福利要有很大的提升");
        record.setRtime(new Timestamp(2000000000));
        boolean i=recordService.add(record);
        logger.info(i);
        logger.info(record.getMid()+"\t"+record.getContent());
    }
}
