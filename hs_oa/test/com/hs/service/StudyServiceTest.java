package com.hs.service;

import com.hs.po.Study;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author awei
 * 学习内容业务逻辑测试类
 */
public class StudyServiceTest {
    StudyService studyService = null;

    Logger logger = Logger.getLogger(StudyServiceTest.class);

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        studyService = context.getBean(StudyService.class);
    }
    @Test
    public void findAll(){
        List<Study> list=studyService.findAll();
        for (Study study:list){
            logger.info(study.getStutitle());
        }
    }
    @Test
    public void findByStuid(){
        Study study=studyService.findByStuid(1);
        logger.info(study.getStutitle());
    }
    @Test
    public void addStudy(){
        Study study=new Study();
        study.setStutitle("语文");
        study.setStuaddr("www.baidu.com");
        boolean flag=studyService.addStudy(study);
        logger.info(flag);
    }
    @Test
    public void update(){
        Study study=studyService.findByStuid(6);
        study.setStutitle("数学");
        boolean flag=studyService.update(study);
        logger.info(flag);
    }
    @Test
    public void delete(){
        boolean flag=studyService.delete(6);
        logger.info(flag);
    }
}
