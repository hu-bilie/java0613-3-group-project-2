package com.hs.service;

import com.hs.dao.BookmarksDao;
import com.hs.dao.BookmarksDaoTest;
import com.hs.po.Bookmarks;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author 陈晨
 * 收藏夹业务逻辑测试
 */
public class BookmarksServiceTest {
    Logger logger=Logger.getLogger(BookmarksDaoTest.class);

    BookmarksService bookmarksService=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/*.xml");
        bookmarksService=applicationContext.getBean(BookmarksService.class);
    }

    @Test
    public void delete(){
        boolean i=bookmarksService.delete(5);
        logger.info(i);
        this.findAll();
    }

    @Test
    public void update(){
        Bookmarks bookmarks=bookmarksService.findByBmid(2);
        bookmarks.setBmname("前端");
        boolean i=bookmarksService.update(bookmarks);
        logger.info(i);
        this.findAll();
    }

    @Test
    public void findByAid(){
        List<Bookmarks> list=bookmarksService.findByAid(10001);
        for(Bookmarks bookmarks:list){
            logger.info(bookmarks.getBmname());
        }
    }

    @Test
    public void findByBmid(){
        Bookmarks bookmarks=bookmarksService.findByBmid(2);
        logger.info(bookmarks.getBmname());
    }

    @Test
    public void findAll(){
        List<Bookmarks> list=bookmarksService.findAll();
        for(Bookmarks bookmarks:list){
            logger.info(bookmarks.getBmid()+"\t"+bookmarks.getBmname()+"\t"+bookmarks.getAid());
        }
    }

    @Test
    public void add(){
        Bookmarks bookmarks=new Bookmarks();
        bookmarks.setBmname("web");
        bookmarks.setAid(10001);
        boolean i=bookmarksService.add(bookmarks);
        logger.info(i);
        logger.info(bookmarks.getBmid()+"\t"+bookmarks.getBmname()+"\t"+bookmarks.getAid());
    }
}
