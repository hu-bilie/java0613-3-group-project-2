package com.hs.service;

import com.hs.dao.MeetingDao;
import com.hs.dao.MeetingDaoTest;
import com.hs.po.Meeting;
import com.hs.vo.MeetingVO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author 陈晨
 * 会议业务逻辑测试类
 */
public class MeetingServiceTest {
    Logger logger=Logger.getLogger(MeetingDaoTest.class);
    MeetingService meetingService=null;

    @Before
    public void common(){
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/*.xml");
        logger.info(applicationContext);
        meetingService=applicationContext.getBean(MeetingService.class);
    }

    @Test
    public void update(){
        Meeting meeting=meetingService.findByMid(2);
        meeting.setDid(4);
        meeting.setMtitle("教师节活动");
        meeting.setMeetingstart(new Timestamp(2000000000));
        meeting.setMeetingstop(new Timestamp(2000100000));
        boolean i=meetingService.update(meeting);
        logger.info(i);
        this.findAll();
    }

    @Test
    public void findVOByMeetingstart(){
        List<MeetingVO> list=meetingService.findVOByMeetingstart(new Timestamp(12000000));
        for(MeetingVO meeting:list){
            logger.info(meeting);
        }
    }
    @Test
    public void findByMeetingstart(){
        List<Meeting> list=meetingService.findByMeetingstart(new Timestamp(12000000));
        for(Meeting meeting:list){
            logger.info(meeting);
        }
    }

    @Test
    public void findVOByMid(){
        MeetingVO meeting=meetingService.findVOByMid(1);
        logger.info(meeting);
    }
    @Test
    public void findByMid(){
        Meeting meeting=meetingService.findByMid(1);
        logger.info(meeting);
    }

    @Test
    public void findVOByMtitle(){

        List<MeetingVO> list=meetingService.findVOByMtitle("秋");
        for(MeetingVO meeting:list){
            logger.info(meeting);
        }
    }
    @Test
    public void findByMtitle(){

        List<Meeting> list=meetingService.findByMtitle("秋");
        for(Meeting meeting:list){
            logger.info(meeting);
        }
    }

    @Test
    public void add(){
        Meeting meeting=new Meeting();
        meeting.setMtitle("中秋节福利");
        meeting.setDid(2);
        meeting.setMeetingstart(new Timestamp(12000000));
        meeting.setMeetingstop(new Timestamp(1200000000));
        boolean i=meetingService.add(meeting);
        logger.info(i);
        this.findVOAll();
    }

    @Test
    public void findVOAll(){
        List<MeetingVO> list=meetingService.findVOAll();
        for(MeetingVO meeting:list){
            logger.info(meeting);
        }
    }
    @Test
    public void findAll(){
        List<Meeting> list=meetingService.findAll();
        for(Meeting meeting:list){
            logger.info(meeting);
        }
    }
}
