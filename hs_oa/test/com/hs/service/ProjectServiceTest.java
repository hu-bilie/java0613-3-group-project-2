package com.hs.service;

import com.hs.po.Project;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author bilie
 * 项目立项业务逻辑测试类
 */
public class ProjectServiceTest {

    ProjectService projectService = null;

    Logger logger = Logger.getLogger(ProjectServiceTest.class);

    @Before
    public void common(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-*.xml");
        logger.info(context);
        projectService = context.getBean(ProjectService.class);
    }

    @Test
    public void findAll(){
        List<Project> list = projectService.findAll();
        for (Project project:list){
            logger.info(project);
        }
    }

    @Test
    public void findByEid(){
        List<Project> list = projectService.findByEid(1);
        for (Project project:list){
            logger.info(project);
        }
    }


    @Test
    public void add(){
        Project project = new Project();
        project.setPname("第二个项目");
        project.setPcontent("第二个项目的内容");
        project.setEid(1);
        project.setPpid(1);
        project.setAsid(1);
        projectService.add(project);
        findAll();
    }

    @Test
    public void update(){
        Project project = projectService.findByPid(3);
        logger.info(project);
        project.setPcontent("修改第二个项目内容");
        projectService.update(project);
        findAll();
    }

    @Test
    public void count(){
        logger.info(projectService.count());
    }

}
